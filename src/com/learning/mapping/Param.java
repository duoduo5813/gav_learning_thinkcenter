package com.learning.mapping;

public class Param {

	public static String POSTGRESQL_USERNAME 			= "kun";
	public static String POSTGRESQL_DATABASE 			= "gavlearning";
	public static String POSTGRESQL_CRITICAL_DATABASE 	= "critical";
	public static String POSTGRESQL_PASSWORD    		= "63030898";
	public static String POSTGRESQL_CONNECTION  		= "jdbc:postgresql://127.0.0.1:5432";
	public static String POSTGRESQL_COMPLETE_CONNECTION = POSTGRESQL_CONNECTION + "/" + POSTGRESQL_DATABASE;
	public static String POSTGRESQL_COMPLETE_CONNECTION_TO_CRITICAL = POSTGRESQL_CONNECTION + "/" + POSTGRESQL_CRITICAL_DATABASE;

	
	// realted to the generation of GAV constraint
	public static String VARIABLE_PREFIX = "x";
	public static String AND_STR 		 = "&";
	public static String KEYWORD_AND     = " AND ";
	public static String KEYWORD_VARCHAR = " VARCHAR(128) ";
	public static String KEYWORD_WHERE 	 = " WHERE ";
	public static String EMTPY_MAPPING   = "emptymapping";
	
	// related to tables in POSTGRESQL
	public static String TABLE_ATTRIBUTE_PREFIX = "attr";
	
	// generate universal examples
	public static String NEWLINE						= "\n";
	public static String UNIVERSAL_EXAMPLE_DELIMITER 	= "#";
	public static String UNIVERSAL_SOURCE_INSTANCE_TAG	= "source:";
	public static String UNIVERSAL_TARGET_INSTANCE_TAG	= "target:";

}
