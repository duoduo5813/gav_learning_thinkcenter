package com.learning.mapping;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.learning.example.*;

public class GAVconstraint {

	ArrayList<Atom> premise;
	Atom conclusion;
	String datasourceTag ="";
	
	
	public String getDatasourceTag() {
		return datasourceTag;
	}



	public void setDatasourceTag(String datasourceTag) {
		this.datasourceTag = datasourceTag;
	}



	public GAVconstraint(){}



	/**
	 * @param premise
	 * @param conclusion
	 */
	public GAVconstraint(ArrayList<Atom> premise, Atom conclusion) {
		super();
		this.premise = premise;
		this.conclusion = conclusion;
	}
	
	public String getConstraint()
	{
		StringBuffer sb = new StringBuffer();
		for(Atom atom : premise)
		{
			sb.append(" "+ atom+" ");
			sb.append(Param.AND_STR);
		}
		sb.deleteCharAt(sb.length()-1);
		return String.format("%s -> %s", sb.toString(), conclusion);
	}
	
	public String getConstraintAsSQL()
	{
		String CQTemplate = "SELECT %s FROM %s WHERE %s";
		
		StringBuffer attrList = new StringBuffer();
		StringBuffer fromList = new StringBuffer();
		StringBuffer whereList = new StringBuffer();
		
		if(conclusion==null)
		{
			return Param.EMTPY_MAPPING;
		}
		
		// e.g. GAV as SQL=[ S(x1,x2) & R(x2,x3)  -> T(x1,x3) ; datasourceTag=1]
		for(int i = 0; i<conclusion.getArity(); i++)
		{
			String currAttr = conclusion.getVars()[i];
			boolean found = false;
			for(Atom atom : premise)
			{
				String relationSymbolInSource = atom.getRelationSymbol();
//				String sourceTagInSource = tableTag;
				for(int k = 0; k< atom.getArity(); k++)
				{
					String attrInSource = atom.getVars()[k];
					if(currAttr.equals(attrInSource))
					{
						// found this attribute
						// create attribute list
						String newSelectedAttr = String.format("%s.%s%d", 
											relationSymbolInSource,
											Param.TABLE_ATTRIBUTE_PREFIX, k+1);
						attrList.append(newSelectedAttr);
						attrList.append(",");
						found=true;
						break;
					}
				}
				if(found)
					break;
			}
		}
		
		
		
		
		String attrClause = attrList.deleteCharAt(attrList.length()-1).toString();
		return String.format(CQTemplate, attrClause,"", "");
	}
	
	public boolean allRelationsAreDistinct(String tableTag)
	{
		HashSet<String> distinctRelations = new HashSet<String>();
		for(int k = 0; k < premise.size(); k++)
		{
			// go through every atom in the premise part
			Atom currAtom = premise.get(k);
			if(distinctRelations.contains(currAtom.getRelationSymbol()))
			{
				return false;
			}
			else
			{
				distinctRelations.add(currAtom.getRelationSymbol());
			}
		}
		return true;
	}
	
	private int computeAliasIndex(HashMap<String, ArrayList<Integer>> occurrenceIndexes, String relationName, int index){
		ArrayList<Integer> list = occurrenceIndexes.get(relationName);
		for(int k = 0; k < list.size(); k++)
		{
			if(list.get(k)==index)
				return k+1;
		}
		return -1;
	}
	public String getConstraintAsSQL(String tableTag)
	{
		String CQTemplate = "SELECT %s FROM %s %s";
		
		StringBuffer attrList = new StringBuffer();
		StringBuffer fromList = new StringBuffer();
		StringBuffer whereList = new StringBuffer();
		
		if(conclusion==null)
		{
			return Param.EMTPY_MAPPING;
		}
		
		if(!allRelationsAreDistinct(tableTag))
		{			
			HashMap<String, String> relationToAlias 	= new HashMap<String, String>();
			HashMap<String, Integer> aliasAvailableIndex = new HashMap<String, Integer>();
			HashMap<String, ArrayList<Integer>> occurrenceIndexes = new HashMap<String, ArrayList<Integer>>();

			// prepare data source alias
			final String ALIAS_PREFIX = "alias";
			final String ALIAS_SUFFIX = "_alias";
			for(int k = 0; k < premise.size(); k++)
			{
				// go through every atom in the premise part
				Atom currAtom = premise.get(k);
				if(!relationToAlias.containsKey(currAtom.getRelationSymbol()))
				{
					// this relation never shows up
					relationToAlias.put(currAtom.getRelationSymbol(), ALIAS_PREFIX+"1");
					aliasAvailableIndex.put(currAtom.getRelationSymbol(), 1);
					ArrayList<Integer> indexes = new ArrayList<Integer>();
					indexes.add(k);
					occurrenceIndexes.put(currAtom.getRelationSymbol(), indexes);
				}
				else
				{
					int previousIdx = aliasAvailableIndex.get(currAtom.getRelationSymbol());
					relationToAlias.put(currAtom.getRelationSymbol(), ALIAS_PREFIX+Integer.toString(previousIdx+1));
					aliasAvailableIndex.put(currAtom.getRelationSymbol(), previousIdx+1);
					ArrayList<Integer> previousIndexes = occurrenceIndexes.get(currAtom.getRelationSymbol());
					previousIndexes.add(k);
					occurrenceIndexes.put(currAtom.getRelationSymbol(), previousIndexes);
				}
			}
			
			int totalNumOfRelations = 0;
			// create From list
			for(String relation: relationToAlias.keySet())
			{
				int nbrOfOccurrences = aliasAvailableIndex.get(relation);
				if(nbrOfOccurrences < 2)
				{
					// unique relation
					fromList.append(relation);
					fromList.append(",");
					totalNumOfRelations++;
				}
				else
				{
					// more than one occurrence
					// then need to create alias
					for(int i = 1; i <= nbrOfOccurrences; i++)
					{
						// e.g., append "relationName alias1,"
						fromList.append(relation+tableTag);
						fromList.append(" ");
						fromList.append(relation);
						fromList.append(ALIAS_SUFFIX);
						fromList.append(i);
						fromList.append(",");
					}
					totalNumOfRelations+= nbrOfOccurrences;
				}
			}
			
			
			/*
			 * Create WHERE CLAUSE
			 */
			System.out.println("FromList = "+ fromList.toString());
			System.out.println("Total number of relations = " + totalNumOfRelations);
			
			for(int premiseIndex = 0; premiseIndex < premise.size(); premiseIndex++)
			{
				Atom leftAtom = premise.get(premiseIndex);
				for(int j = premiseIndex+1; j < premise.size(); j++)
				{
					Atom rightAtom = premise.get(j);
					for(int k = 0; k < leftAtom.arity; k++)
					{
						for(int l = 0; l < rightAtom.arity; l++)
						{				
							if(leftAtom.getVars()[k].equals(rightAtom.getVars()[l]))
							{
								// found a pair of equal value
								// need to create a new term in WHERE clause
//								System.out.println(String.format("k = %d, l = %d", k, l));
//								System.out.println("\nleftAtom = " + leftAtom);
//								System.out.println("rightAtom = " + rightAtom);
								int aliasIndexLeft  = computeAliasIndex(occurrenceIndexes, leftAtom.getRelationSymbol(), premiseIndex);
								int aliasIndexRight = computeAliasIndex(occurrenceIndexes, rightAtom.getRelationSymbol(), j);
								String newEquality  = String.format("%s%s%s%d.%s%d = %s%s%s%d.%s%d", 
										leftAtom.getRelationSymbol(),
										tableTag,
										ALIAS_SUFFIX,
										aliasIndexLeft,
										Param.TABLE_ATTRIBUTE_PREFIX, k+1,
										rightAtom.getRelationSymbol(),
										tableTag,
										ALIAS_SUFFIX,
										aliasIndexRight,
										Param.TABLE_ATTRIBUTE_PREFIX, l+1);
								whereList.append(newEquality);
								whereList.append(Param.KEYWORD_AND);
//								System.out.println("newEquality = " + newEquality);
							}

						}
					}
				}
			}
			
			/*
			 * Create SELECT clause
			 */
			for(int i = 0; i<conclusion.getArity(); i++)
			{
				String currAttr = conclusion.getVars()[i];
				boolean found = false;
				for(int premiseIndex = 0; premiseIndex < premise.size(); premiseIndex++)
				{
					Atom atom = premise.get(premiseIndex);
					String relationSymbol = atom.getRelationSymbol();
					for(int k = 0; k< atom.getArity(); k++)
					{
						String attrInSource = atom.getVars()[k];
						if(currAttr.equals(attrInSource))
						{
							// found this attribute
							// create attribute list
							int aliasIndex = computeAliasIndex(occurrenceIndexes, relationSymbol, premiseIndex);
							String newSelectedAttr = String.format("%s%s%s%d.%s%d", 
									relationSymbol, 
									tableTag,
									ALIAS_SUFFIX,
									aliasIndex,
									Param.TABLE_ATTRIBUTE_PREFIX, 
									k+1);
							attrList.append(newSelectedAttr);
							attrList.append(",");
							found=true;
							break;
						}
					}
					if(found)
						break;
				}
				
			}
			
			String whereClause = "";

			if(whereList.length()>0)
			{
				whereClause = Param.KEYWORD_WHERE + whereList.substring(0, whereList.length()-Param.KEYWORD_AND.length());
			}
			
			String attrClause = attrList.deleteCharAt(attrList.length()-1).toString();
			String fromClause = fromList.deleteCharAt(fromList.length()-1).toString();
//			System.out.println(String.format(CQTemplate, attrClause, fromClause, whereClause));
//			System.exit(0);
			return String.format(CQTemplate, attrClause, fromClause, whereClause);
		}
		else
		{
			// e.g. GAV as SQL=[ S(x1,x2) & R(x2,x3)  -> T(x1,x3) ; datasourceTag=1]
			for(int i = 0; i < conclusion.getArity(); i++)
			{
				String currAttr = conclusion.getVars()[i];
				boolean found = false;
				for(Atom atom : premise)
				{
					String relationSymbolInSource = atom.getRelationSymbol();
					String sourceTagInSource = tableTag;
					for(int k = 0; k< atom.getArity(); k++)
					{
						String attrInSource = atom.getVars()[k];
						if(currAttr.equals(attrInSource))
						{
							// found this attribute
							// create attribute list
							String newSelectedAttr = String.format("%s%s.%s%d", 
												relationSymbolInSource, sourceTagInSource,
												Param.TABLE_ATTRIBUTE_PREFIX, k+1);
							attrList.append(newSelectedAttr);
							attrList.append(",");
							found=true;
							break;
						}
					}
					if(found)
						break;
				}
			}
			
			// e.g. GAV as SQL=[ S(x1,x2) & R(x2,x3)  -> T(x1,x3) ; datasourceTag=1]
			// constructing FROM clause
			HashSet<String> distinctRelations = new HashSet<String>();
			for(int k = 0; k < premise.size(); k++)
			{
				// go through every atom in the premise part
				Atom currAtom = premise.get(k);
				if(!distinctRelations.contains(currAtom.getRelationSymbol()+tableTag))
				{
					distinctRelations.add(currAtom.getRelationSymbol()+tableTag);
				}
			}
			for(String relation : distinctRelations)
			{
				fromList.append(relation);
				fromList.append(",");
			}
			
			// e.g. GAV as SQL=[ S(x1,x2) & R(x2,x3)  -> T(x1,x3) ; datasourceTag=1]
			// constructing WHERE clause
//			boolean[][] attrMatrix = new boolean[distinctRelations.size()][distinctRelations.size()];
			for(int i = 0; i < premise.size(); i++)
			{
				Atom firstAtom = premise.get(i);
				for(int j = i+1; j < premise.size(); j++)
				{
					Atom secondAtom = premise.get(j);
					for(int k = 0; k < firstAtom.arity; k++)
					{
						for(int l = 0; l < secondAtom.arity; l++)
						{
							if(firstAtom.getVars()[k].equals(secondAtom.getVars()[l]))
							{
								String newEquality = String.format("%s%s.%s%d = %s%s.%s%d", 
										firstAtom.getRelationSymbol(),
										tableTag,
										Param.TABLE_ATTRIBUTE_PREFIX, k+1,
										secondAtom.getRelationSymbol(),
										tableTag,
										Param.TABLE_ATTRIBUTE_PREFIX, l+1);
								whereList.append(newEquality);
								whereList.append(Param.KEYWORD_AND);
//								if(attrMatrix[k][l]!=true)
//								{
//									attrMatrix[k][l] = true;
//									attrMatrix[l][k] = true;
//									String newEquality = String.format("%s%s.%s%d = %s%s.%s%d", 
//											firstAtom.getRelationSymbol(),
//											tableTag,
//											Param.TABLE_ATTRIBUTE_PREFIX, k+1,
//											secondAtom.getRelationSymbol(),
//											tableTag,
//											Param.TABLE_ATTRIBUTE_PREFIX, l+1);
//									whereList.append(newEquality);
//									whereList.append(Param.KEYWORD_AND);
//								}
							}
						}
					}
				}
			}
			String whereClause = "";

			if(whereList.length()>0)
			{
				whereClause = Param.KEYWORD_WHERE + whereList.substring(0, whereList.length()-Param.KEYWORD_AND.length());
			}
			
			String attrClause = attrList.deleteCharAt(attrList.length()-1).toString();
			String fromClause = fromList.deleteCharAt(fromList.length()-1).toString();
			return String.format(CQTemplate, attrClause, fromClause, whereClause);
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conclusion == null) ? 0 : conclusion.hashCode());
		result = prime * result + ((datasourceTag == null) ? 0 : datasourceTag.hashCode());
		result = prime * result + ((premise == null) ? 0 : premise.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GAVconstraint other = (GAVconstraint) obj;
		if (conclusion == null) {
			if (other.conclusion != null)
				return false;
		} else if (!conclusion.equals(other.conclusion))
			return false;
		if (datasourceTag == null) {
			if (other.datasourceTag != null)
				return false;
		} else if (!datasourceTag.equals(other.datasourceTag))
			return false;
		if (premise == null) {
			if (other.premise != null)
				return false;
		} else if (!premise.equals(other.premise))
			return false;
		return true;
	}



	public String toString()
	{
		return String.format("[%s ; datasourceTag=%s]", getConstraint(), datasourceTag);
	}

	// convert the source instance into a conjunctive query 
	// over the source instance
	// this function is used in following circumstances:
	// (1) when a new constraint has been found, convert a pair of 
	//     instance into a GAV constraint

	public static GAVconstraint convertSourceInstanceToCQ(Example example)
	{
		if(example.getTarget().getSize()==0)
			return null;
		GAVconstraint result = new GAVconstraint();
		result.setPremise(Atom.createConjunction(example.getSource()));
		System.out.println("constraint="+ result.getPremise());
		return result;
	}
	

	public static GAVconstraint createConstraint(Example example)
	{
		System.out.println(example.getId());
		GAVconstraint constraint = new GAVconstraint();
		// compute premise part
		ArrayList<Atom> result = new ArrayList<Atom>();
		String var = Param.VARIABLE_PREFIX;
		int counter = 1;
		
		HashMap<String, String> valToVar = new HashMap<String,String>();
		for(Fact fact : example.getSource().getFacts())
		{
			Atom newAtom = new Atom();
			newAtom.setRelationSymbol(fact.getRelationSymbol());
			newAtom.setArity(fact.getArity());
			String[] vars = new String[newAtom.arity];
			for(int i = 0; i < vars.length; i++)
			{
				String val = fact.getTuple()[i];
				if(!valToVar.containsKey(val))
				{
					vars[i] = var+counter;
					valToVar.put(val, vars[i]);
					counter++;
				}
				else
				{
					vars[i] = valToVar.get(val);
				}
			}
			newAtom.setVars(vars);
			result.add(newAtom);
		}
		constraint.setPremise(result);
		
		// compute conclusion part
		Fact currentTargetFact = example.getTarget().getFacts().get(0);
		String[] vars = new String[currentTargetFact.getArity()];
		for(int k = 0; k < currentTargetFact.getArity(); k++)
		{
			String val = currentTargetFact.getTuple()[k];
			if(!valToVar.containsKey(val))
			{
				vars[k] = var+counter;
				valToVar.put(val, vars[k]);
				counter++;
			}
			else
			{
				vars[k] = valToVar.get(val);
			}
		}
		
		Atom newConclusionAtom = new Atom();
		newConclusionAtom.setRelationSymbol(currentTargetFact.getRelationSymbol());
		newConclusionAtom.setVars(vars);
		constraint.setConclusion(newConclusionAtom);
		constraint.setDatasourceTag(example.getDatasourceTag());
		return constraint;
	}

	public static GAVconstraint createConstraint(GavExample example)
	{
		GAVconstraint constraint = new GAVconstraint();
		
		// compute premise part
		ArrayList<Atom> result = new ArrayList<Atom>();
		String var = Param.VARIABLE_PREFIX;
		int counter = 1;
		
		HashMap<String, String> valToVar = new HashMap<String,String>();
		for(Fact fact : example.getSource().getFacts())
		{
			Atom newAtom = new Atom();
			newAtom.setRelationSymbol(fact.getRelationSymbol());
			newAtom.setArity(fact.getArity());
			String[] vars = new String[newAtom.arity];
			for(int i = 0; i < vars.length; i++)
			{
				String val = fact.getTuple()[i];
				if(!valToVar.containsKey(val))
				{
					vars[i] = var+counter;
					valToVar.put(val, vars[i]);
					counter++;
				}
				else
				{
					vars[i] = valToVar.get(val);
				}
			}
			newAtom.setVars(vars);
			result.add(newAtom);
		}
		constraint.setPremise(result);
		
		// compute conclusion part
		Fact currentTargetFact = example.getTargetFact();
		String[] vars = new String[currentTargetFact.getArity()];
		for(int k = 0; k < currentTargetFact.getArity(); k++)
		{
			String val = currentTargetFact.getTuple()[k];
			if(!valToVar.containsKey(val))
			{
				vars[k] = var+counter;
				valToVar.put(val, vars[k]);
				counter++;
			}
			else
			{
				vars[k] = valToVar.get(val);
			}
		}
		
		Atom newConclusionAtom = new Atom();
		newConclusionAtom.setRelationSymbol(currentTargetFact.getRelationSymbol());
		newConclusionAtom.setVars(vars);
		constraint.setConclusion(newConclusionAtom);
		
		constraint.setDatasourceTag(example.getDatasourceTag());
		return constraint;
	}

	public ArrayList<Atom> getPremise() {
		return premise;
	}



	public void setPremise(ArrayList<Atom> premise) {
		this.premise = premise;
	}



	public Atom getConclusion() {
		return conclusion;
	}


	public void setConclusion(Atom conclusion) {
		this.conclusion = conclusion;
	};
	
	
	
}
