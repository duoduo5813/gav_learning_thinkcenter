package com.learning.mapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.learning.example.Example;
import com.learning.example.Fact;
import com.learning.example.Instance;

public class Atom {
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arity;
		result = prime * result + ((relationSymbol == null) ? 0 : relationSymbol.hashCode());
		result = prime * result + Arrays.hashCode(vars);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atom other = (Atom) obj;
		if (arity != other.arity)
			return false;
		if (relationSymbol == null) {
			if (other.relationSymbol != null)
				return false;
		} else if (!relationSymbol.equals(other.relationSymbol))
			return false;
		if (!Arrays.equals(vars, other.vars))
			return false;
		return true;
	}

	String relationSymbol;
	int arity;
	String[] vars;
	
	
	/**
	 * @param relationSymbol
	 * @param arity
	 * @param vars
	 */
	public Atom(String relationSymbol, int arity, String[] vars) {
		super();
		this.relationSymbol = relationSymbol;
		this.arity = arity;
		this.vars = vars;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		for(String var : vars)
		{
			sb.append(var);
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		return String.format("%s(%s)", relationSymbol, sb.toString());
	}
	
	public int getArity() {
		arity = vars.length;
		return arity;
	}

	public void setArity(int arity) {
		this.arity = arity;
	}

	public Atom(){}
	
	
	// S(a,b) R(b,c) -> T(a,c)
	// S(x1,x2) R(x2,x3) -> T(x1,x3)
	// select attr1, attr2 from S and R where  S.attr2 = R.attr1
	public static ArrayList<Atom> createConjunction(Example example)
	{
		ArrayList<Atom> result = new ArrayList<Atom>();
		String var = Param.VARIABLE_PREFIX;
		int counter = 1;
		

		
		HashMap<String, String> valToVar = new HashMap<String,String>();
		for(Fact fact : example.getSource().getFacts())
		{
			Atom newAtom = new Atom();
			newAtom.setRelationSymbol(fact.getRelationSymbol());
			newAtom.setArity(fact.getArity());
			String[] vars = new String[newAtom.arity];
			for(int i = 0; i < vars.length; i++)
			{
				String val = fact.getTuple()[i];
				if(!valToVar.containsKey(val))
				{
					vars[i] = var+counter;
					valToVar.put(val, vars[i]);
					counter++;
				}
				else
				{
					vars[i] = valToVar.get(val);
				}
			}
			newAtom.setVars(vars);
			result.add(newAtom);
		}
		return result;
	}
	
	
	public static ArrayList<Atom> createConjunction(Instance sourceInstance)
	{
		ArrayList<Atom> result = new ArrayList<Atom>();
		String var = Param.VARIABLE_PREFIX;
		int counter = 1;
		
		HashMap<String, String> valToVar = new HashMap<String,String>();
		for(Fact fact : sourceInstance.getFacts())
		{
			Atom newAtom = new Atom();
			newAtom.setRelationSymbol(fact.getRelationSymbol());
			newAtom.setArity(fact.getArity());
			String[] vars = new String[newAtom.arity];
			for(int i = 0; i < vars.length; i++)
			{
				String val = fact.getTuple()[i];
				if(!valToVar.containsKey(val))
				{
					vars[i] = var+counter;
					valToVar.put(val, vars[i]);
					counter++;
				}
				else
				{
					vars[i] = valToVar.get(val);
				}
			}
			newAtom.setVars(vars);
			result.add(newAtom);
		}
		return result;
	}
	
	public String[] getVars() {
		return vars;
	}

	public void setVars(String[] vars) {
		this.vars = vars;
	}

	public static ArrayList<Atom> createConjunction(ArrayList<Fact> sourceFacts)
	{
		ArrayList<Atom> result = new ArrayList<Atom>();
		String var = Param.VARIABLE_PREFIX;
		int counter = 1;
		
		HashMap<String, String> valToVar = new HashMap<String,String>();
		for(Fact fact : sourceFacts)
		{
			Atom newAtom = new Atom();
			newAtom.setRelationSymbol(fact.getRelationSymbol());
			newAtom.setArity(fact.getArity());
			String[] vars = new String[newAtom.arity];
			for(int i = 0; i < vars.length; i++)
			{
				String val = fact.getTuple()[i];
				if(!valToVar.containsKey(val))
				{
					vars[i] = var+counter;
					valToVar.put(val, vars[i]);
					counter++;
				}
				else
				{
					vars[i] = valToVar.get(val);
				}
			}
		}
		return result;
	}

	public String getRelationSymbol() {
		return relationSymbol;
	}

	public void setRelationSymbol(String relationSymbol) {
		this.relationSymbol = relationSymbol;
	};
	
	
	
	
	
	
}
