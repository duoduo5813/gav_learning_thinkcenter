package com.learning.mapping;

import java.util.*;

public class Relation {
	
	String relationSymbol ;
	ArrayList<String> attributes;
	
	public Relation()
	{}

	public String getRelationSymbol() {
		return relationSymbol;
	}

	public void setRelationSymbol(String relationSymbol) {
		this.relationSymbol = relationSymbol;
	}

	public ArrayList<String> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<String> attributes) {
		this.attributes = attributes;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(relationSymbol+"(");
		for(String attr : attributes)
		{
			sb.append(attr);
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		return sb.append(")").toString();
	}
}
