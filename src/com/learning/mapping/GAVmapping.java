package com.learning.mapping;

import com.learning.example.*;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import org.postgresql.util.PSQLException;

public class GAVmapping implements Serializable {

	public HashSet<GavExample> gavExamples;
	public HashSet<GAVconstraint> gavConstraints;
	
	public GAVmapping()
	{
		gavExamples = new HashSet<GavExample>();
		gavConstraints = new HashSet<GAVconstraint>();
	};
	
	public GAVmapping makeCopy()
	{
		GAVmapping newMapping = new GAVmapping();
		newMapping.gavConstraints = gavConstraints;
		newMapping.gavExamples = gavExamples;
		return newMapping;
	}
	public String toReadableString()
	{
		StringBuffer sb = new StringBuffer();
		
		HashMap<String, String> symbolMap = new HashMap<String, String>();
		int upperbound = 10;      
		String base = "A"; 
		int startIndex = 0;
		
		for(GAVconstraint c : gavConstraints)
		{
			GAVconstraint readable = new GAVconstraint();
			ArrayList<Atom> readablePremise = new ArrayList<Atom>();
			for(Atom atom : c.premise)
			{
				String rel = atom.getRelationSymbol();
				if(symbolMap.containsKey(rel))
				{
					Atom newReadable = new Atom();
					newReadable.setArity(atom.arity);
					newReadable.setVars(atom.vars);
					newReadable.setRelationSymbol(symbolMap.get(rel));
					readablePremise.add(newReadable);
				}
				else
				{
					Atom newReadable = new Atom();
					newReadable.setArity(atom.arity);
					newReadable.setVars(atom.vars);
					String newRel = "";

					if(startIndex < upperbound)
					{
						startIndex++;
						newRel = String.format("%s%d", base, startIndex);
						symbolMap.put(rel, newRel);
					}
					else
					{
						startIndex = 0;
						char baseC = base.charAt(0);
						char newBase = (char)(baseC + 1);
						base = Character.toString(newBase);
						newRel = String.format("%s%d", base, startIndex);
						symbolMap.put(rel, newRel);
					}
					newReadable.setRelationSymbol(newRel);
					readablePremise.add(newReadable);
				}
			}
			
			// for conclusion part
			Atom atom = c.conclusion;
			String rel = atom.getRelationSymbol();
			Atom newReadableConclusion = null;
			if(symbolMap.containsKey(rel))
			{
				Atom newReadable = new Atom();
				newReadable.setArity(atom.arity);
				newReadable.setVars(atom.vars);
				newReadable.setRelationSymbol(symbolMap.get(rel));
				newReadableConclusion = newReadable;
			}
			else
			{
				Atom newReadable = new Atom();
				newReadable.setArity(atom.arity);
				newReadable.setVars(atom.vars);
				String newRel = "";

				if(startIndex < upperbound)
				{
					startIndex++;
					newRel = String.format("%s%d", base, startIndex);
					symbolMap.put(rel, newRel);
				}
				else
				{
					startIndex = 0;
					char baseC = base.charAt(0);
					char newBase = (char)(baseC + 1);
					base = Character.toString(newBase);
					newRel = String.format("%s%d", base, startIndex);
					symbolMap.put(rel, newRel);
				}
				newReadable.setRelationSymbol(newRel);
				newReadableConclusion = newReadable;
			}
			readable.setPremise(readablePremise);
			readable.setConclusion(newReadableConclusion);

			sb.append(readable);
			sb.append(";");
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		
		for(GAVconstraint c : gavConstraints)
		{
			sb.append(c);
			sb.append(";");
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	public void addConstraint(GavExample example)
	{
		gavExamples.add(example);
		GAVconstraint newC = GAVconstraint.createConstraint(example);
		gavConstraints.add(newC);
	}
	
	public void addConstraint(GAVconstraint newConstraint)
	{
		gavConstraints.add(newConstraint);
	}	
	
	public static HashSet<Fact> chase(GAVmapping mapping, Example example)
	{
//		System.out.println("CHASING = "+ example);

		HashSet<Fact> result = new HashSet<Fact>();
//		System.out.println("mapping = " + mapping);
		for(GavExample gavExample : mapping.gavExamples)
		{
			GAVconstraint constraint = GAVconstraint.createConstraint(gavExample);
//			System.out.println("trying + " + constraint);
			try{
				String SQL = constraint.getConstraintAsSQL(example.getDatasourceTag());
//				System.out.println(String.format("Running %s\nSQL=%s on\nExample = %s", constraint, SQL, example));
				ArrayList<Fact> tmp = runSQL(gavExample, SQL, example);
				if(tmp!=null)
				{
//					System.out.println("obtained by chasing="+ tmp);
					result.addAll(tmp);
				}
			}
			catch(Exception e)
			{
				System.out.println("XXXXX has problem with : "+ gavExample);
				String SQL = constraint.getConstraintAsSQL(example.getDatasourceTag());
				System.out.println("Problem is  = " + SQL);
			}

		}		
		return result;
	}
	
	
	
	public static ArrayList<Fact> runSQL(GavExample gavExample, String script, Example universalExample)
	{
		ArrayList<Fact> chaseResult = new ArrayList<Fact>();
		try 
		{
			Class.forName("org.postgresql.Driver");
		} 
		catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
		}
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(
					Param.POSTGRESQL_COMPLETE_CONNECTION, 
					Param.POSTGRESQL_USERNAME,
					Param.POSTGRESQL_PASSWORD);
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(script);
			int arity = rs.getMetaData().getColumnCount();
			while ( rs.next() )
			{
				Fact newFact = new Fact();
				newFact.setRelationSymbol(gavExample.getTargetFact().getRelationSymbol());
				newFact.setArity(gavExample.getTargetFact().getArity());
				String[] vals = new String[newFact.getArity()];
				for(int i = 1; i <= arity; i++)
				{
//					System.out.print(rs.getString(i)+" ");
					vals[i-1] = rs.getString(i);
				}
				newFact.setTuple(vals);
				chaseResult.add(newFact);
//				System.out.println();
			}
		    rs.close();
		    st.close();
		    connection.close();
		    connection = null;
		} 
		catch(PSQLException e)
		{
			if(e.getMessage().contains("does not exist"))
			{
//				System.out.println("Does not exist");
				return null;
			}
		}
		catch(Exception e){e.printStackTrace();}
		
		return chaseResult;
	}
	
	
	
	
	
	public static HashSet<Fact> chaseCritical(GAVmapping mapping, Example example)
	{
		HashSet<Fact> result = new HashSet<Fact>();
		
		for(GavExample gavExample : mapping.gavExamples)
		{
			GAVconstraint constraint = GAVconstraint.createConstraint(gavExample);
			String SQL = constraint.getConstraintAsSQL(example.getDatasourceTag());
			System.out.println(String.format("Running %s\nSQL=%s on\nExample = %s", constraint, SQL, example));
			ArrayList<Fact> tmp = runSQL_Critical(gavExample, SQL, example);
			if(tmp!=null)
			{
				System.out.println("obtained by chasing="+ tmp);
				result.addAll(tmp);
			}
		}		
		return result;
	}
	
	public static ArrayList<Fact> runSQL_Critical(GavExample gavExample, String script, Example universalExample)
	{
		ArrayList<Fact> chaseResult = new ArrayList<Fact>();
		try 
		{
			Class.forName("org.postgresql.Driver");
		} 
		catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
		}
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(
					Param.POSTGRESQL_COMPLETE_CONNECTION_TO_CRITICAL, 
					Param.POSTGRESQL_USERNAME,
					Param.POSTGRESQL_PASSWORD);
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(script);
			int arity = rs.getMetaData().getColumnCount();
			while ( rs.next() )
			{
				Fact newFact = new Fact();
				newFact.setRelationSymbol(gavExample.getTargetFact().getRelationSymbol());
				newFact.setArity(gavExample.getTargetFact().getArity());
				String[] vals = new String[newFact.getArity()];
				for(int i = 1; i <= arity; i++)
				{
					System.out.print(rs.getString(i)+" ");
					vals[i-1] = rs.getString(i);
				}
				newFact.setTuple(vals);
				chaseResult.add(newFact);
				System.out.println();
			}
		    rs.close();
		    st.close();
		    connection.close();
		    connection = null;
		} 
		catch(PSQLException e)
		{
			if(e.getMessage().contains("does not exist"))
			{
				System.out.println("Does not exist");
				return null;
			}
		}
		catch(Exception e){e.printStackTrace();}
		
		return chaseResult;
	}
	
	
	public static ArrayList<Fact> chase(GAVmapping mapping, ArrayList<Example> examples)
	{
		ArrayList<Fact> result = new ArrayList<Fact>();
		
		for(GavExample gavExample : mapping.gavExamples)
		{
			GAVconstraint constraint = GAVconstraint.createConstraint(gavExample);
			for(Example example : examples)
			{
				String SQL = constraint.getConstraintAsSQL(example.getDatasourceTag());
				System.out.println(String.format("Running GavEx=%s AS SQL=%s\nExample=%s", gavExample, SQL, example));
			}
		}		
		return result;
	}
	
}
