package com.learning.mapping;

import java.util.ArrayList;

public class Schema {
	
	ArrayList<Relation> relations;
	
	
	public Schema()
	{
		relations = new ArrayList<Relation>();
	}

	public void addRelation(Relation newRel)
	{
		relations.add(newRel);
	}	
	
	public ArrayList<Relation> getRelations() {
		return relations;
	}

	public void setRelations(ArrayList<Relation> relations) {
		this.relations = relations;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("\n# relations = %s\n", relations.size()));
		for(Relation r : relations)
		{
			sb.append(r+"\n");
		}
		return sb.toString();
	}
	
}
