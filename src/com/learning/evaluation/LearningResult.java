package com.learning.evaluation;

import java.util.ArrayList;
import java.util.HashMap;

import com.learning.mapping.GAVconstraint;

public class LearningResult {

	double precision ; 
	double recall	 ;
	double runtime   ; 
	double maxPrec   ;
	double minPrec   ;
	double degreeOfComp_Training ;
	double degreeOfComp_Evaluation;
	String detailedComprehensivenessInfo;
	int totalNumberOfTriggers = 0;
	int totalNumberOfIterations = 0;
	
	public void setTotalNumberOfIterations(int i)
	{
		totalNumberOfIterations = i;
	}
	HashMap<GAVconstraint, Integer> triggeredStatistics;
	
	public String getDetailedComprehensivenessInfo() {
		return detailedComprehensivenessInfo;
	}
	public void setDetailedComprehensivenessInfo(String detailedComprehensivenessInfo) {
		this.detailedComprehensivenessInfo = detailedComprehensivenessInfo;
	}
	public HashMap<GAVconstraint, Integer> getTriggeredStatistics() {
		return triggeredStatistics;
	}
	public void setTriggeredStatistics(HashMap<GAVconstraint, Integer> triggeredStatistics) {
		this.triggeredStatistics = triggeredStatistics;
		int numOfTriggeredConstraints = 0;
		for(GAVconstraint c : triggeredStatistics.keySet())
		{
			int tmp = triggeredStatistics.get(c);
			if(tmp>0)
				numOfTriggeredConstraints++;
			totalNumberOfTriggers += tmp;
		}
		double doc = (double)numOfTriggeredConstraints / (double)triggeredStatistics.size();
		this.setDegreeOfComp_Training(doc);
	}
	
	
	
	public double getDegreeOfComp_Training() {
		return degreeOfComp_Training;
	}
	public void setDegreeOfComp_Training(double degreeOfComp_Training) {
		this.degreeOfComp_Training = degreeOfComp_Training;
	}
	public double getDegreeOfComp_Evaluation() {
		return degreeOfComp_Evaluation;
	}
	public void setDegreeOfComp_Evaluation(double degreeOfComp_Evaluation) {
		this.degreeOfComp_Evaluation = degreeOfComp_Evaluation;
	}
	public double getMaxPrec() {
		return maxPrec;
	}
	public void setMaxPrec(double maxPrec) {
		this.maxPrec = maxPrec;
	}
	public double getMinPrec() {
		return minPrec;
	}
	public void setMinPrec(double minPrec) {
		this.minPrec = minPrec;
	}
	public double getMaxRecall() {
		return maxRecall;
	}
	public void setMaxRecall(double maxRecall) {
		this.maxRecall = maxRecall;
	}
	public double getMinRecall() {
		return minRecall;
	}
	public void setMinRecall(double minRecall) {
		this.minRecall = minRecall;
	}

	double maxRecall ;
	double minRecall ;
	
	boolean isTrainComprehensive = true;
	
	public boolean isTrainComprehensive() {
		return isTrainComprehensive;
	}
	public void setTrainComprehensive(boolean isComprehensive) {
		this.isTrainComprehensive = isComprehensive;
	}
	public double getRuntime() {
		return runtime;
	}
	public void setRuntime(double runtime) {
		this.runtime = runtime;
	}
	public double getPrecision() {
		return precision;
	}
	public void setPrecision(double precision) {
		this.precision = precision;
	}
	public double getRecall() {
		return recall;
	}
	public void setRecall(double recall) {
		this.recall = recall;
	}
	
	
	public String getRunningTime()
	{
//		System.out.println("runtime="+ runtime);
		double runTimeSec = runtime/1000.0;
//		System.out.println("runtimeSec="+ runTimeSec);
		int min = (int)(runTimeSec/60);
		double remainingSecs =  (runTimeSec - (min*60.0f));
		return String.format(" %dmin %.1f secs", min, remainingSecs);
//		return String.format(" %d min %.2f sec", min, remainingSecs);
	}
	
	public static double avgDegreeOfComprehensiveness(ArrayList<LearningResult> results)
	{
		double sum = 0;
		for(LearningResult lr : results)
		{
			sum+= lr.getDegreeOfComp_Training();
		}
		return sum/(double)results.size();
	}
	
	public static double avgLearningIteration(ArrayList<LearningResult> results)
	{
		double avg = 0;
		for(LearningResult lr : results)
		{
			avg += lr.totalNumberOfIterations;
		}
		return avg/(double)results.size();
	}
	
	public static double avgNumTriggers(ArrayList<LearningResult> results)
	{
		double avg = 0;
		for(LearningResult lr : results)
		{
			avg += lr.totalNumberOfTriggers;
		}
		return avg/(double)results.size();
	}
	
	public String computeDetailedComprehensiveness()
	{
		StringBuffer sb = new StringBuffer();
		
		for(GAVconstraint constraint : triggeredStatistics.keySet())
		{
			sb.append(String.format("[%d]->%s\n",triggeredStatistics.get(constraint), constraint));
		}
		detailedComprehensivenessInfo = sb.toString();
		return sb.toString();
	}
	
	public String toString()
	{
//		StringBuffer sb = new StringBuffer();
//		sb.append("Precision =" + this.getPrecision());
//		sb.append("\t Recall =" + this.getRecall());
//		sb.append("\t Runtime =" + this.getRunningTime());
//		sb.append("\t Comprehensiveness = " + this.isTrainComprehensive());
//		return sb.toString();
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("Precision=%.4f", this.getPrecision()));
		sb.append(String.format("\tRecall=%.5f", this.getRecall()));
		sb.append(String.format("\tDOC_TRAIN=%.3f" , this.getDegreeOfComp_Training()));
		sb.append("\tRuntime =" + this.getRunningTime());
		sb.append("\nTotal Number of LearningIterations = " + totalNumberOfIterations);
		sb.append("\nTotal Number of triggers = " + totalNumberOfTriggers);
		sb.append("\n"+ computeDetailedComprehensiveness());
//		sb.append(String.format("\tDOC_EVAL=%.3f" , this.getDegreeOfComp_Evaluation()));

//		sb.append("\t Comprehensiveness = " + this.isTrainComprehensive());
//
//		sb.append("Precision =" + this.getPrecision());
//		sb.append("\t Recall =" + this.getRecall());
//		sb.append("\t Runtime =" + this.getRunningTime());
//		sb.append("\t Comprehensiveness = " + this.isTrainComprehensive());
		return sb.toString();
	}
	public LearningResult(){}
	
	public static String avgRuntime(ArrayList<LearningResult> results)
	{
		double sumOfRuntime = 0;
		for(LearningResult lr : results)
		{
			sumOfRuntime += lr.getRuntime();
		}
		double avg = sumOfRuntime/((double)(results.size()));
		return convertToReadableRumtime(avg);
	}
	
	public static String convertToReadableRumtime(double r)
	{
		double runTimeSec = r/1000.0;
//		System.out.println("runtimeSec="+ runTimeSec);
		int min = (int)(runTimeSec/60.0f);
		double remainingSecs =  (runTimeSec - (min*60.0f));
		return String.format(" %dmin %.1f secs", min, remainingSecs);
	}
	
	public static double avgPrecision(ArrayList<LearningResult> results)
	{
		double sum = 0;
		for(LearningResult result : results)
		{
			sum += result.getPrecision();
			
		}
//		System.out.println("computing sum = " + sum + "/" + (double)results.size() + "="+ sum/((double)results.size()));
		return sum/((double)results.size());
	}
	
	public static double computePrecisionDeviation(ArrayList<LearningResult> results, double avgPrec)
	{
		double N = results.size();
		// deviation = sqrt( (E1-avg)^2 + ... + (En-avg) /N);
		double denominator = 0;
		for(LearningResult result : results)
		{
			double tmp = (result.getPrecision() - avgPrec) * (result.getPrecision() - avgPrec);
			denominator += tmp;
		}
		return Math.sqrt(denominator/N);
	}
	
	public static double computeRecallDeviation(ArrayList<LearningResult> results, double avgRecall)
	{
		double N = results.size();
		// deviation = sqrt( (E1-avg)^2 + ... + (En-avg) /N);
		double denominator = 0;
		for(LearningResult result : results)
		{
			double tmp = (result.getRecall() - avgRecall) * (result.getRecall() - avgRecall);
			denominator += tmp;
		}
		return Math.sqrt(denominator/N);
	}
//	public static PrecisionDeviation computePrecisionDeviation(ArrayList<LearningResult> results)
//	{
//		double minPrec = 1;
//		double maxPrec = 0;
//		PrecisionDeviation deviation = new PrecisionDeviation();
//		for(LearningResult result : results)
//		{
//			minPrec = Math.min(minPrec, result.getPrecision());
//			maxPrec = Math.max(maxPrec, result.getPrecision());
//		}
////		System.out.println("computing sum = " + sum + "/" + (double)results.size() + "="+ sum/((double)results.size()));
//		deviation.setMaxPrecision(maxPrec);
//		deviation.setMinPrecision(minPrec);
//		return deviation;
//	}
//	
//	public static RecallDeviation computeRecallDeviation(ArrayList<LearningResult> results)
//	{
//		double minRecall = 1;
//		double maxRecall = 0;
//		RecallDeviation deviation = new RecallDeviation();
//		for(LearningResult result : results)
//		{
//			minRecall = Math.min(minRecall, result.getRecall());
//			maxRecall = Math.max(maxRecall, result.getRecall());
//		}
////		System.out.println("computing sum = " + sum + "/" + (double)results.size() + "="+ sum/((double)results.size()));
//		deviation.setMinRecall(minRecall);
//		deviation.setMaxRecall(maxRecall);
//		return deviation;
//	}
	
	public static double avgRecall(ArrayList<LearningResult> results)
	{
		double sum = 0;
		for(LearningResult result : results)
		{
			sum += result.getRecall();
		}
		return sum/((double)results.size());
	}
	
	public static double computeFscore(double precision, double recall)
	{
		return (2.0f*precision*recall)/(precision+recall);
	}
}
