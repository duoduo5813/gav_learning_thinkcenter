package com.learning.evaluation;

import com.learning.mapping.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.learning.example.*;
import com.learning.ibench.adapters.iBenchMappingParser;
import com.learning.ibench.adapters.iBenchMappingScenario;
import com.learning.learn.GavLearner;
import com.learning.learn.OptimizedPostgresConnector;

public class mappingEvaluator {

	GAVmapping goal;
	GAVmapping learned;
	ArrayList<Example> testExamples;
	
	public mappingEvaluator(iBenchMappingScenario scenario, String pathTolearnedMapping)
	{
		goal = scenario.getGoalMapping();
		learned = GavExample.LoadGoalMapping(pathTolearnedMapping);
		
		System.out.println("Eval = " + goal);
		System.out.println();
		System.out.println("Eval = " + learned);
	}
	
	public mappingEvaluator(String pathToTestSet, String pathTolearnedMapping)
	{
		learned = GavExample.LoadGoalMapping(pathTolearnedMapping);
		testExamples = Example.BatchLoad(pathToTestSet);
	}
	
	public LearningResult computePrecision_Recall_Fscore()
	{
		double truePositives = 0;
		double falsePositives = 0;
		double falseNegatives = 0;
		
		double precision = 0;
		double recall = 0;
		
		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		
		int count = 0;
		System.out.println("Evaluation started.");
		
		for(Example example :testExamples)
		{
			if(count % 1 == 0)
			{
				System.out.print(count+String.format("[size=%d]", example.getSource().getFacts().size())+" ");
			}
			Example newC = new Example();
			Instance newSource = new Instance(example.getSource().getFacts());
			Instance newTarget = new Instance(new ArrayList<Fact>());
			newC.setSource(newSource);
			newC.setTarget(newTarget);
			
			Set<String> relations = newC.getSource().getFactMap().keySet();
		
			postgresConnector.populateNewTableDDL(newC);
			
			HashSet<Fact> chased = GAVmapping.chase(learned, newC);
			
//			System.out.println("chased = " + chased);
			// compute precision
			// precision = true positives / size of chased 
			ArrayList<Fact> truePos = getTruePositives(example.getTarget().getFacts(), chased);
			int localFalsePositives = chased.size()-truePos.size();
			truePositives += truePos.size();
//			System.out.println("truePos = " + truePos.size());
			falsePositives += localFalsePositives;
			
			// recall = true positives / true positives + false negatives;
			ArrayList<Fact> falseNeg = getFalseNegatives(example.getTarget().getFacts(), chased);
//			System.out.println("falseNeg = " + falseNeg.size());
			falseNegatives += falseNeg.size();
			count++;
		}
		postgresConnector.closeUp();
//		System.out.println("\nEvaluation finished");
//		System.out.println(String.format("#TruePos. = %f\t#FalsePos. = %f\t#FalseNeg. = %f\n", 
//				truePositives, 
//				falsePositives,
//				falseNegatives));

		precision = truePositives / (truePositives+falsePositives);
		recall 	  = truePositives / (truePositives+falseNegatives);
		double fScore = (2*precision*recall)/(precision+recall);
		
		LearningResult lr = new LearningResult();
		lr.setPrecision(precision);
		lr.setRecall(recall);
		return lr;
		
//		System.out.println(String.format("Precision = %f\nRecall = %f\nF-Score = %f\n", precision, recall, fScore));

	}
	
	public ArrayList<Fact> getFalseNegatives(ArrayList<Fact> universal, HashSet<Fact> chased)
	{
		ArrayList<Fact> falseNeg = new ArrayList<Fact>();
		
		for(Fact f : universal)
		{
			if(!chased.contains(f))
				falseNeg.add(f);
		}
		return falseNeg;
	}
	
	public ArrayList<Fact> getTruePositives(ArrayList<Fact> universal, HashSet<Fact> chased)
	{
		ArrayList<Fact> truePos = new ArrayList<Fact>();
		for(Fact f : chased)
		{
			if(universal.contains(f))
			{
				truePos.add(f);
			}
		}
		return truePos;
	}
	
	public static void main(String[] args)
	{
		String pathToLearned =  "/Users/kunqian/git/gav_learning/GAVLearning/learnedMappings/learnedMapping";
		String pathToTestSet =  "/Users/kunqian/git/gav_learning/GAVLearning/src/COMPTEST";
		String pathToProperties = "/Users/kunqian/git/gav_learning/GAVLearning/learningConfigComp";
		iBenchMappingParser iBenchParser = new iBenchMappingParser(pathToProperties);
		iBenchMappingScenario scenario = iBenchParser.parse();
		
		mappingEvaluator evaluator = new mappingEvaluator(pathToTestSet, pathToLearned);
		evaluator.computePrecision_Recall_Fscore();
	}
	
	
}
