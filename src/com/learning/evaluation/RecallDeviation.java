package com.learning.evaluation;

public class RecallDeviation {
	
	public RecallDeviation(){}
	
	public double minRecall = 0;
	public double maxRecall = 0;
	public double getMinRecall() {
		return minRecall;
	}
	public void setMinRecall(double minRecall) {
		this.minRecall = minRecall;
	}
	public double getMaxRecall() {
		return maxRecall;
	}
	public void setMaxRecall(double maxRecall) {
		this.maxRecall = maxRecall;
	}
	
	public String toString()
	{
		return String.format("[%.3f,%.3f]", minRecall, maxRecall );
	}
}
