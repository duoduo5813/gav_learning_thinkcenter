package com.learning.evaluation;

import com.learning.ibench.adapters.iBenchMappingParser;
import com.learning.ibench.adapters.iBenchMappingScenario;

public class randomScenarioEvaluationMain {

	public static void main(String[] args)
	{
		String pathToLearned =  "/Users/kunqian/git/gav_learning/GAVLearning/learnedMappings/learnedMapping";
		String pathToTestSet =  "/Users/kunqian/git/gav_learning/GAVLearning/src/RANDTEST";
		String pathToProperties = "/Users/kunqian/git/gav_learning/GAVLearning/learningConfigComp";
//		iBenchMappingParser iBenchParser = new iBenchMappingParser(pathToProperties);
//		iBenchMappingScenario scenario = iBenchParser.parse();
		
		mappingEvaluator evaluator = new mappingEvaluator(pathToTestSet, pathToLearned);
		evaluator.computePrecision_Recall_Fscore();
	}
}
