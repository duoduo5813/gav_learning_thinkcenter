package com.learning.evaluation;

public class PrecisionDeviation {
	double minPrecision =0 ;
	double maxPrecision =0 ;
	public double getMinPrecision() {
		return minPrecision;
	}
	public void setMinPrecision(double minPrecision) {
		this.minPrecision = minPrecision;
	}
	public double getMaxPrecision() {
		return maxPrecision;
	}
	public void setMaxPrecision(double maxPrecision) {
		this.maxPrecision = maxPrecision;
	}
	
	public String toString()
	{
		return String.format("[%.3f,%.3f]", minPrecision, maxPrecision );
	}
	
	public PrecisionDeviation(){}
}
