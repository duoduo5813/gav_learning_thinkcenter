package com.learning.example;
import java.util.ArrayList;
import com.learning.mapping.*;

public class learningMain {

	public static void main(String[] args)
	{
		ArrayList<Example> examples = Example.BatchLoad("/Users/kunqian/coding/workspace/GAVLearning/src/examples1");
		
		for(Example e : examples)
		{
			System.out.println(e);
			System.out.println(e.getSource().getFactMap());
			System.out.println(e.getTarget().getFactMap());
			GAVconstraint newConstraint = GAVconstraint.convertSourceInstanceToCQ(e);
			if(newConstraint!=null)
				System.out.println(newConstraint.getConstraint());
			if(e.isGavExample())
			{
				GAVconstraint gav = GAVconstraint.createConstraint(e);
				System.out.println("GAV as constraint="+gav);
				System.out.println("GAV as SQL=" + gav.getConstraintAsSQL(e.getDatasourceTag()));
			}
			System.out.println();
		}
		
	}
}
