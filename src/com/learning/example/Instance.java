package com.learning.example;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Instance implements Serializable {
	private static final long serialVersionUID = 7526472295622776145L;

	
	ArrayList<Fact> facts;
	HashMap<String, ArrayList<Fact>> factMap;
	int size;
	boolean isEmpty = true;
	
	public Instance(ArrayList<Fact> facts)
	{
		this.facts = facts;
		populateFactMap(facts);
		size = facts.size();
		isEmpty = size==0 ? true : false;
	}
	
	private void populateFactMap(ArrayList<Fact> facts)
	{
		factMap = new HashMap<String, ArrayList<Fact>>();
		for(Fact f : facts){
			if(!factMap.containsKey(f.getRelationSymbol()))
			{
				// encounter a fact that belongs to an unseen relation
				ArrayList<Fact> tmp = new ArrayList<Fact>();
				tmp.add(f);
				factMap.put(f.getRelationSymbol(), tmp);
			}
			else
			{
				ArrayList<Fact> previous = factMap.get(f.getRelationSymbol());
				if(!previous.contains(f))
				{
					previous.add(f);
					factMap.put(f.getRelationSymbol(), previous);
				}
			}
		}
	}
	
	public HashMap<String, ArrayList<Fact>> getFactMap() {
		return factMap;
	}

	public void setFactMap(HashMap<String, ArrayList<Fact>> factMap) {
		this.factMap = factMap;
	}
	
	public String toString()
	{
		if(facts==null || facts.size()==0)
		{
			return "{empty}";
		}
		StringBuffer sb = new StringBuffer();
		for(Fact f: facts)
		{
			sb.append(f);
			sb.append("; ");
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((factMap == null) ? 0 : factMap.hashCode());
		result = prime * result + ((facts == null) ? 0 : facts.hashCode());
		result = prime * result + (isEmpty ? 1231 : 1237);
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Instance other = (Instance) obj;
		if (factMap == null) {
			if (other.factMap != null)
				return false;
		} else if (!factMap.equals(other.factMap))
			return false;
		if (facts == null) {
			if (other.facts != null)
				return false;
		} else if (!facts.equals(other.facts))
			return false;
		if (isEmpty != other.isEmpty)
			return false;
		if (size != other.size)
			return false;
		return true;
	}

	public boolean isEmpty() {
		return facts.size()==0;
	}

	public Instance(){}

	public ArrayList<Fact> getFacts() {
		return facts;
	}

	public void setFacts(ArrayList<Fact> facts) {
		populateFactMap(facts);
		this.facts = facts;
	}

	public int getSize() {
		return facts.size();
	}

	
}
