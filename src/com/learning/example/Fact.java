package com.learning.example;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Fact implements Serializable{
	private static final long serialVersionUID = 7526472295622776142L;

	String relationSymbol;
	int arity;
	String[] tuple;
	
	public static boolean hasNonemptyIntersection(Fact f1, Fact f2)
	{
		HashSet<String> values = new HashSet<String>();
		for(String val : f1.tuple)
		{
			values.add(val);
		}
		for(String val : f2.tuple)
		{
			if(values.contains(val))
				return true;
		}		
		return false;
	}
	
	public Fact(String relation, String[] values)
	{
		relationSymbol = relation;
		tuple = values;
		arity = values.length;
	}
	public Fact(){};
	
	public void setTuples(ArrayList<String> vals){
		tuple = new String[vals.size()];
		arity  = vals.size();
		for(int i = 0; i < vals.size(); i++)
		{
			tuple[i] = vals.get(i);
		}
	}
	
	public static Fact createFact(String relationSymbol, ArrayList<String> values)
	{
		Fact newF = new Fact();
		newF.setRelationSymbol(relationSymbol);
		newF.setTuples(values);
		return newF;
	}
	
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		for(String val : tuple)
		{
			sb.append(val);
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		return String.format("%s(%s)", relationSymbol, sb.toString());
	}
	
	public static Fact parseFact(String factStr)
	{
//		System.out.println("parsing = " + factStr);
		if(factStr.length()<=0)
		{
			Fact newFact = new Fact();
			return newFact;
		}
		// e.g., Student(Joe, Doe, 23, M)
		Fact newFact = new Fact();
		newFact.setRelationSymbol(factStr.substring(0, factStr.indexOf("(")));
		newFact.setTuple(factStr.substring(factStr.indexOf("(")+1, factStr.indexOf(")")).split(","));
		return newFact;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arity;
		result = prime * result + ((relationSymbol == null) ? 0 : relationSymbol.hashCode());
		result = prime * result + Arrays.hashCode(tuple);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fact other = (Fact) obj;
		if (arity != other.arity)
			return false;
		if (relationSymbol == null) {
			if (other.relationSymbol != null)
				return false;
		} else if (!relationSymbol.equals(other.relationSymbol))
			return false;
		if (!Arrays.equals(tuple, other.tuple))
			return false;
		return true;
	}
	public String getRelationSymbol() {
		return relationSymbol;
	}

	public void setRelationSymbol(String relationSymbol) {
		this.relationSymbol = relationSymbol;
	}

	public int getArity() {
		return arity;
	}

	public void setArity(int arity) {
		this.arity = arity;
	}

	public String[] getTuple() {
		
		return tuple;
		
	}

	public void setTuple(String[] tuple) {
		this.tuple = tuple;
		this.arity = tuple.length;
	}
}
