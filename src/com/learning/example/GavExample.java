package com.learning.example;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

import com.learning.mapping.GAVmapping;

public class GavExample extends Example implements Serializable {
	
	private static final long serialVersionUID = 7526472295622776144L;

	
	Fact targetFact;
	
	public GavExample()
	{
		super();
	}

	public String toString()
	{
		return String.format("%s\nSource:%s\nTarget:%s\n", id, source, targetFact);
	}
	
	public static GAVmapping LoadGoalMapping(String file)
	{
		GAVmapping goalMapping = new GAVmapping();
		
		ArrayList<Example> result = new ArrayList<Example>();
		
		FileInputStream fis;
		try 
		{
			fis = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line;
			Example currentExample = null;
			while((line=br.readLine())!=null)
			{
				if(line.trim().equals("#"))
				{
					if(currentExample!=null)
						result.add(currentExample);
					currentExample = new Example();
				}
				else
				{
					if(line.startsWith(sourcePrefix))
					{
						String meat = line.substring(sourcePrefix.length());
						String[] rawFacts = meat.split(" ");
						Instance newSource = new Instance();
						ArrayList<Fact> sourceTuples = new ArrayList<Fact>();
						for(String rawFactStr : rawFacts)
						{
							Fact newFact = Fact.parseFact(rawFactStr);
							sourceTuples.add(newFact);
						}
						newSource.setFacts(sourceTuples);
						currentExample.setSource(newSource);
					}
					else if(line.startsWith(targetPrefix))
					{
						String meat = line.substring(targetPrefix.length());
						Instance newTarget = new Instance();
						ArrayList<Fact> targetTuples = new ArrayList<Fact>();
						if(meat.length()>0)
						{
							String[] rawFacts = meat.split(" ");
							for(String rawFactStr : rawFacts)
							{
								Fact newFact = Fact.parseFact(rawFactStr);
								targetTuples.add(newFact);
							}
						}
						newTarget.setFacts(targetTuples);
						currentExample.setTarget(newTarget);
					}
				}
			}
			if(currentExample!=null)
			{
				result.add(currentExample);
			}
			
			br.close();
			fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(Example e : result)
		{
			if(e.isGavExample())
			{
				GavExample newGavExample = new GavExample();
				newGavExample.setSource(e.getSource());
				newGavExample.setTargetFact(e.getTarget().getFacts().get(0));
				goalMapping.addConstraint(newGavExample);
			}
		}
		return goalMapping;
	}
	
	@Override
	public void setTarget(Instance target) {
		// TODO Auto-generated method stub
		if(target.getFacts().size()!=1)
		{
			System.err.println("The number of target facts does not equals to 1");
			System.exit(1);
		}
		targetFact = target.getFacts().get(0);
		super.setTarget(target);
	}

	public Fact getTargetFact() {
		return targetFact;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((targetFact == null) ? 0 : targetFact.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GavExample other = (GavExample) obj;
		if (targetFact == null) {
			if (other.targetFact != null)
				return false;
		} else if (!targetFact.equals(other.targetFact))
			return false;
		return true;
	}

	public void setTargetFact(Fact targetFact) {
		this.targetFact = targetFact;
	}
	
	

}
