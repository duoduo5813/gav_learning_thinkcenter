package com.learning.example;
import java.io.*;
import java.util.ArrayList;

public class Example implements Serializable {
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datasourceTag == null) ? 0 : datasourceTag.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Example other = (Example) obj;
		if (datasourceTag == null) {
			if (other.datasourceTag != null)
				return false;
		} else if (!datasourceTag.equals(other.datasourceTag))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}

	private static final long serialVersionUID = 7526472295622776147L;
	
	final static String sourcePrefix ="source:";
	final static String targetPrefix ="target:";
	Instance source;
	Instance target;
	String id;
	String datasourceTag = "";
	
	static int counter =1;
	
	public Example()
	{
		id = String.format("(I_%d,J_%d)", counter, counter);
		datasourceTag = Integer.toString(counter);
		counter++;
	}
	
	public String getDatasourceTag() {
		return datasourceTag;
	}

	public void setDatasourceTag(String datasourceTag) {
		id = String.format("(I_%s,J_%s)", datasourceTag, datasourceTag);
		this.datasourceTag = datasourceTag;
	}

	public String getId() {
		return id;
	}

	public boolean isGavExample()
	{
		return target.getSize()==1;
	}

	public void setId(String id) {
		this.id = id;
	}


	public static int getCounter() {
		return counter;
	}


	public static void setCounter(int counter) {
		Example.counter = counter;
	}


	public static String getSourceprefix() {
		return sourcePrefix;
	}


	public static String getTargetprefix() {
		return targetPrefix;
	}
	

	public static ArrayList<Example> BatchLoad(String file)
	{
		ArrayList<Example> result = new ArrayList<Example>();
		
		FileInputStream fis;
		try 
		{
			fis = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line;
			Example currentExample = null;
			while((line=br.readLine())!=null)
			{
				if(line.startsWith("%"))
					continue;
				if(line.trim().equals("#"))
				{
					if(currentExample!=null)
						result.add(currentExample);
					currentExample = new Example();
				}
				else
				{
					if(line.startsWith(sourcePrefix))
					{
						String meat = line.substring(sourcePrefix.length());
						String[] rawFacts = meat.split(" ");
						Instance newSource = new Instance();
						ArrayList<Fact> sourceTuples = new ArrayList<Fact>();
						for(String rawFactStr : rawFacts)
						{
							Fact newFact = Fact.parseFact(rawFactStr);
							sourceTuples.add(newFact);
						}
						newSource.setFacts(sourceTuples);
						currentExample.setSource(newSource);
					}
					else if(line.startsWith(targetPrefix))
					{
						String meat = line.substring(targetPrefix.length());
						Instance newTarget = new Instance();
						ArrayList<Fact> targetTuples = new ArrayList<Fact>();
						if(meat.length()>0)
						{
							String[] rawFacts = meat.split(" ");
							for(String rawFactStr : rawFacts)
							{
								Fact newFact = Fact.parseFact(rawFactStr);
								targetTuples.add(newFact);
							}
						}
						newTarget.setFacts(targetTuples);
						currentExample.setTarget(newTarget);
					}
				}
			}
			if(currentExample!=null)
			{
				result.add(currentExample);
			}
			
			br.close();
			fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public Instance getSource() {
		return source;
	}

	public void setSource(Instance source) {
		this.source = source;
	}

	public Instance getTarget() {
		return target;
	}



	public void setTarget(Instance target) {
		this.target = target;
	}

	public Example(String id, Instance s, Instance t)
	{
		this.id = id;
		source = s;
		target = t;
	}
	
	public Example(Instance s, Instance t)
	{
		id = String.format("(I_%d,J_%d)", counter, counter);
		counter++;
		source = s;
		target = t;
	}
	
	public String toString()
	{
		return String.format("%s\nSource:%s\nTarget:%s\n", id, source, target);
	}
	

}
