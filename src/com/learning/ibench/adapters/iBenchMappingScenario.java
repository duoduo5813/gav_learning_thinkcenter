package com.learning.ibench.adapters;

import com.learning.example.Example;
import com.learning.example.Fact;
import com.learning.example.GavExample;
import com.learning.example.Instance;
import com.learning.learn.Keywords;
import com.learning.learn.OptimizedPostgresConnector;
import com.learning.learn.PostgresConnector;
import com.learning.mapping.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.*;

public class iBenchMappingScenario implements Serializable {

	private static final long serialVersionUID = 7526472295622776146L;

	GAVmapping goalMapping;
	Schema sourceSchema;
	Schema targetSchema;
	HashMap<String, ArrayList<Fact>> sourceFactsByRelation;

	Properties config = new Properties();

	String IBENCH_ROOT = null;
	String IBENCH_MAPPING_OUT_FOLDER = null;
	String IBENCH_MAPPING_CONFIG_FILENAME = null;
	String WORK_ROOT = null;
	String UNIVERSAL_EXAMPLE_C_PATH = null;
	String MAPPING_SCENARIO_FILENAME = null;

	public String getPathToC() {
		return WORK_ROOT + "/" + UNIVERSAL_EXAMPLE_C_PATH;
	}
	
	
	public void buildConfigFileForiBench(String templatePath, String configOutDir, int suffix)
	{
		String configOutPath = configOutDir.endsWith("/") ? (configOutDir+Keywords.COMPREHENSIVE_SCENARIOS_FOLDER+"/"+Keywords.CONFIG_FILENAME):
							(configOutDir+"/"+Keywords.COMPREHENSIVE_SCENARIOS_FOLDER+"/"+Keywords.CONFIG_FILENAME);
		configOutPath += suffix;
//		System.out.println(templatePath);
		String template	 		 = readConfigTemplate(templatePath);
		Configuration parameters = new Configuration();
		String configFile 		 = createConfigFile(template, parameters);
//		System.out.println(configFile);
		try 
		{
//			System.out.println("XXX="+configOutPath);
			FileWriter outWriter = new FileWriter(configOutPath);
			outWriter.write(configFile);
			outWriter.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("configuration file is successfully created");	
	}
	
	public void buildConfigFileForiBench_random(String templatePath, String configOutDir, int suffix)
	{
		String configOutPath = configOutDir.endsWith("/") ? (configOutDir+Keywords.RANDOM_SCENARIOS_FOLDER+"/"+Keywords.CONFIG_FILENAME):
							(configOutDir+"/"+Keywords.RANDOM_SCENARIOS_FOLDER+"/"+Keywords.CONFIG_FILENAME);
		configOutPath += suffix;
		System.out.println("templatePath="+templatePath);
		String template	 		 = readConfigTemplate(templatePath);
		Configuration parameters = new Configuration();
		String configFile 		 = createConfigFile(template, parameters);
//		System.out.println(configFile);
		try 
		{
			System.out.println("creating configuration file for the random scenario="+configOutPath);
			FileWriter outWriter = new FileWriter(configOutPath);
			outWriter.write(configFile);
			outWriter.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("configuration file is successfully created");	
	}
	public void buildConfigFileForiBench_random2(String templatePath, 
			String configOutDir, String configFileName, int suffix, long randomSeed)
	{
		String configOutPath = configOutDir.endsWith("/") ? (configOutDir+Keywords.RANDOM_SCENARIOS_FOLDER+"/"+configFileName):
							(configOutDir+"/"+Keywords.RANDOM_SCENARIOS_FOLDER+"/"+configFileName);
//		configOutPath += suffix;
//		System.out.println("templatePath="+templatePath);
		String template	 		 = readConfigTemplate(templatePath);
		Configuration parameters = new Configuration();
		String configFile 		 = createConfigFileRefined(template, configFileName, parameters, randomSeed);
//		System.out.println(configFile);
		try 
		{
//			System.out.println("creating configuration file for the random scenario="+configOutPath);
			FileWriter outWriter = new FileWriter(configOutPath);
			outWriter.write(configFile);
			outWriter.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("configuration file is successfully created");	
	}
	
	public String readConfigTemplate(String path)
	{
		StringBuffer sb = new StringBuffer();
		
		try 
		{
			FileInputStream fis = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);
			
			String line = "";
			while((line=br.readLine())!=null)
			{
				sb.append(line+"\n");
			}	
			br.close();
			isr.close();
			fis.close();
		}		
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	String parameterSettings = "";
	public void setParameters(String param)
	{
		parameterSettings = param;
	}
	public String getParameters()
	{
		return parameterSettings;
	}
	public String createConfigFile(String template, Configuration parameters)
	{
		// # copy, merging, projection, randomSeed, joinSize(1-10), 
		// # repElementCount(max. 5), ReuseSourcePerc, ReuseTargetPerc
		StringBuffer sb = new StringBuffer();
//		int nbrOfCopying 	= randomWithLowerBound(5, simpleRandom(parameters.getNumOfCopy()));
//		int nbrOfMerging 	= randomWithLowerBound(parameters.numOfMerging/2, parameters.numOfMerging);
//		int nbrOfSelfJoin 	= simpleRandom(parameters.numSelfJoin);
//		int nbrOfProjection = simpleRandom(parameters.numbOfProjection);
//		int nbrOfJoinSize	= randomWithLowerBound(parameters.joinSize/3, parameters.joinSize);
//		int factPerRelatin 	= randomWithLowerBound(nbrOfFactPerRelation_Lowerbound, parameters.numFactPerRelation);
//		int reuseSource     = randomWithLowerBound(parameters.ReuseSourceLowerbound, parameters.ReuseSourcePerc);
//		int reuseTarget 	= randomWithLowerBound(parameters.ReuseTargetLowerbound, parameters.ReuseTargetPerc);
		
		int nbrOfCopying 	= 6;
		int nbrOfMerging 	= 8;
		int nbrOfSelfJoin 	= 0;
		int nbrOfProjection = 6;
		int nbrOfJoinSize	= 6;
		int factPerRelatin 	= 20;
		int reuseSource     = 100;
		int reuseTarget 	= 100;
//		int nbrOfCopying 	= 3;
//		int nbrOfMerging 	= 3;
//		int nbrOfSelfJoin 	= 0;
//		int nbrOfProjection = 3;
//		int nbrOfJoinSize	= 4;
//		int factPerRelatin 	= 5;
//		int reuseSource     = 100;
//		int reuseTarget 	= 100;
		
		sb.append(String.format("nbrOfCopying=%d\n", nbrOfCopying));
		sb.append(String.format("nbrOfMerging=%d\n", nbrOfMerging));
		sb.append(String.format("nbrOfSelfJoin=%d\n", nbrOfSelfJoin));
		sb.append(String.format("nbrOfProjection=%d\n", nbrOfProjection));
		sb.append(String.format("nbrOfJoinSize=%d\n", nbrOfJoinSize));
		sb.append(String.format("reuseSource=%d\n", reuseSource));
		sb.append(String.format("reuseTarget=%d\n", reuseTarget));
		
//		System.out.println(sb.toString());
		setParameters(sb.toString());
//		System.out.println(parameterSettings);
	
		return String.format(template, 
				nbrOfCopying,
				nbrOfMerging, 
				parameters.numSelfJoin,
				nbrOfProjection, 
				simpleRandom(parameters.randomSeed),
				nbrOfJoinSize,
				factPerRelatin,
				reuseSource, 
				parameters.ReuseTargetLowerbound);
//				randomWithLowerBound(parameters.ReuseTargetLowerbound, parameters.ReuseTargetPerc));
	}
	
	public String createConfigFileRefined(String template, String configFileName,  Configuration parameters,
			long randomSeed)
	{
		// # copy, merging, projection, randomSeed, joinSize(1-10), 
		// # repElementCount(max. 5), ReuseSourcePerc, ReuseTargetPerc
		StringBuffer sb = new StringBuffer();
//		int nbrOfCopying 	= randomWithLowerBound(5, simpleRandom(parameters.getNumOfCopy()));
//		int nbrOfMerging 	= randomWithLowerBound(parameters.numOfMerging/2, parameters.numOfMerging);
//		int nbrOfSelfJoin 	= simpleRandom(parameters.numSelfJoin);
//		int nbrOfProjection = simpleRandom(parameters.numbOfProjection);
//		int nbrOfJoinSize	= randomWithLowerBound(parameters.joinSize/3, parameters.joinSize);
//		int factPerRelatin 	= randomWithLowerBound(nbrOfFactPerRelation_Lowerbound, parameters.numFactPerRelation);
//		int reuseSource     = randomWithLowerBound(parameters.ReuseSourceLowerbound, parameters.ReuseSourcePerc);
//		int reuseTarget 	= randomWithLowerBound(parameters.ReuseTargetLowerbound, parameters.ReuseTargetPerc);
		
		/**
		 * 
		 * THis is the one used in the experiments
		 */
		int nbrOfCopying 	= 3;
		int nbrOfMerging 	= 4;
		int nbrOfSelfJoin 	= 0;
		int nbrOfProjection = 3;
		int nbrOfJoinSize	= 3;
		int factPerRelatin 	= 10;
		int reuseSource     = 100;
		int reuseTarget 	= 100;
//		int nbrOfCopying 	= 3;
//		int nbrOfMerging 	= 3;
//		int nbrOfSelfJoin 	= 0;
//		int nbrOfProjection = 3;
//		int nbrOfJoinSize	= 4;
//		int factPerRelatin 	= 5;
//		int reuseSource     = 100;
//		int reuseTarget 	= 100;
		
		sb.append(String.format("nbrOfCopying=%d\n", nbrOfCopying));
		sb.append(String.format("nbrOfMerging=%d\n", nbrOfMerging));
		sb.append(String.format("nbrOfSelfJoin=%d\n", nbrOfSelfJoin));
		sb.append(String.format("nbrOfProjection=%d\n", nbrOfProjection));
		sb.append(String.format("nbrOfJoinSize=%d\n", nbrOfJoinSize));
		sb.append(String.format("reuseSource=%d\n", reuseSource));
		sb.append(String.format("reuseTarget=%d\n", reuseTarget));
		
//		System.out.println(sb.toString());
		setParameters(sb.toString());
//		System.out.println(parameterSettings);
	
		return String.format(template, 
				configFileName,
				configFileName,
				nbrOfCopying,
				nbrOfMerging, 
				parameters.numSelfJoin,
				nbrOfProjection, 
				randomSeed,
				nbrOfJoinSize,
				factPerRelatin,
				reuseSource, 
				parameters.ReuseTargetLowerbound);
//				randomWithLowerBound(parameters.ReuseTargetLowerbound, parameters.ReuseTargetPerc));
	}
	
//	public String createConfigFile(String template, Configuration parameters)
//	{
//		// # copy, merging, projection, randomSeed, joinSize(1-10), 
//		// # repElementCount(max. 5), ReuseSourcePerc, ReuseTargetPerc
//		int nbrOfFactPerRelation_Lowerbound = 3;
//		return String.format(template, 
//				simpleRandom(parameters.getNumOfCopy()),
//				simpleRandom(parameters.numOfMerging), 
//				parameters.numSelfJoin,
//				simpleRandom(parameters.numbOfProjection), 
//				simpleRandom(parameters.randomSeed),
//				simpleRandom(parameters.joinSize), 
//				randomWithLowerBound(nbrOfFactPerRelation_Lowerbound, parameters.numFactPerRelation),
//				randomWithLowerBound(parameters.ReuseSourceLowerbound, parameters.ReuseSourcePerc), 
//				parameters.ReuseTargetLowerbound);
////				randomWithLowerBound(parameters.ReuseTargetLowerbound, parameters.ReuseTargetPerc));
//	}
	
	// simple random(x) returns a random number between 0 and x. 
	public int simpleRandom(int x)
	{
		return (int)(Math.random()*(x+1));
	}
	
	public long simpleRandom(long x)
	{
		return (long)(Math.random()*(x+1));
	}
	
	public int randomWithLowerBound(int lowerbound, int upperbound)
	{
		int r = (int)(Math.random()*(upperbound+1));
		return (r>=lowerbound) ? r : lowerbound;
	}


	public void materializeRandomUniversalsToC(iBenchMappingScenario scenario, ArrayList<Example> examples) {
		// template
		// #
		// source:S(a,b) R(b,c)
		// target:T(a,c)
		//
		// #
		// source:M(a,b) N(b,c) S(a,b) R(b,c)
		// target:Q(a,b,c) T(a,c)

		StringBuffer sb = new StringBuffer();

		for (Example example : examples) {
//			HashSet<Fact> chased = ChaseToGenerateC(scenario, example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getTarget().getFacts()));
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
//		System.out.println(sb.toString());
		String pathToC = WORK_ROOT + UNIVERSAL_EXAMPLE_C_PATH;
		try {
			FileWriter out = new FileWriter(pathToC);
			out.write(sb.toString());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void materializeRandomUniversalsToTRAIING_DATA(iBenchMappingScenario scenario,
			ArrayList<Example> examples, String trainingDataFileName) {
		// template
		// #
		// source:S(a,b) R(b,c)
		// target:T(a,c)
		//
		// #
		// source:M(a,b) N(b,c) S(a,b) R(b,c)
		// target:Q(a,b,c) T(a,c)

		StringBuffer sb = new StringBuffer();

		for (Example example : examples) {
//			HashSet<Fact> chased = ChaseToGenerateC(scenario, example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getTarget().getFacts()));
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
//		System.out.println(sb.toString());
		String pathToC = WORK_ROOT + UNIVERSAL_EXAMPLE_C_PATH;
		String pathToTraining = WORK_ROOT + Keywords.TRAINING_DATA_FOLDER +"/"+
					trainingDataFileName;
		pathToTrainingData = pathToTraining;
		try {
			FileWriter out = new FileWriter(pathToTraining);
			out.write(sb.toString());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public String getPathToTrainingData() {
		return pathToTrainingData;
	}


	public void setPathToTrainingData(String pathToTrainingData) {
		this.pathToTrainingData = pathToTrainingData;
	}


	public String getPathToTestData() {
		return pathToTestData;
	}


	public void setPathToTestData(String pathToTestData) {
		this.pathToTestData = pathToTestData;
	}


	public void materializeTestExamplesToTest(iBenchMappingScenario scenario, ArrayList<Example> examples) {
		// template
		// #
		// source:S(a,b) R(b,c)
		// target:T(a,c)
		//
		// #
		// source:M(a,b) N(b,c) S(a,b) R(b,c)
		// target:Q(a,b,c) T(a,c)

		StringBuffer sb = new StringBuffer();

		for (Example example : examples) {
//			HashSet<Fact> chased = ChaseToGenerateC(scenario, example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getTarget().getFacts()));
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
//		System.out.println(sb.toString());
		String pathToC = WORK_ROOT + Keywords.UNIVERSAL_EXAMPLE_TEST_COMPREHENSIVE;
		try {
			FileWriter out = new FileWriter(pathToC);
			out.write(sb.toString());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void materializeRandTestExamplesToTest_REFINED(iBenchMappingScenario scenario, 
			ArrayList<Example> examples, String testFileName) {
		// template
		// #
		// source:S(a,b) R(b,c)
		// target:T(a,c)
		//
		// #
		// source:M(a,b) N(b,c) S(a,b) R(b,c)
		// target:Q(a,b,c) T(a,c)

		StringBuffer sb = new StringBuffer();

		for (Example example : examples) {
//			HashSet<Fact> chased = ChaseToGenerateC(scenario, example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getTarget().getFacts()));
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
//		System.out.println(sb.toString());
//		String pathToC = WORK_ROOT + Keywords.UNIVERSAL_EXAMPLE_TEST_RANDOM;
		String pathToTest= WORK_ROOT + Keywords.TEST_DATA_FOLDER +"/"+
				testFileName;
		pathToTestData = pathToTest;
		try {
			System.out.println("TRYING = " + pathToTest);
			FileWriter out = new FileWriter(pathToTest);
			out.write(sb.toString());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	String pathToTrainingData = "";
	String pathToTestData	  = "";
	
	public void materializeRandTestExamplesToTest(iBenchMappingScenario scenario, ArrayList<Example> examples) {
		// template
		// #
		// source:S(a,b) R(b,c)
		// target:T(a,c)
		//
		// #
		// source:M(a,b) N(b,c) S(a,b) R(b,c)
		// target:Q(a,b,c) T(a,c)

		StringBuffer sb = new StringBuffer();

		for (Example example : examples) {
//			HashSet<Fact> chased = ChaseToGenerateC(scenario, example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(example.getTarget().getFacts()));
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
//		System.out.println(sb.toString());
		String pathToC = WORK_ROOT + Keywords.UNIVERSAL_EXAMPLE_TEST_RANDOM;
		try {
			FileWriter out = new FileWriter(pathToC);
			out.write(sb.toString());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createAndWriteUniversalExamplesToC(iBenchMappingScenario scenario, ArrayList<Example> examples) {
		// template
		// #
		// source:S(a,b) R(b,c)
		// target:T(a,c)
		//
		// #
		// source:M(a,b) N(b,c) S(a,b) R(b,c)
		// target:Q(a,b,c) T(a,c)

		StringBuffer sb = new StringBuffer();

		for (Example example : examples) {
			HashSet<Fact> chased = ChaseToGenerateC(scenario, example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(scenario.getAllSourceFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(formattingUniversalInstance(chased));
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
		System.out.println(sb.toString());
		String pathToC = WORK_ROOT + UNIVERSAL_EXAMPLE_C_PATH;
		try {
			FileWriter out = new FileWriter(pathToC);
			out.write(sb.toString());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Properties getConfig() {
		return config;
	}

	public void setConfig(Properties config) {
		this.config = config;
	}

	public String getIBENCH_ROOT() {
		return IBENCH_ROOT;
	}

	public void setIBENCH_ROOT(String iBENCH_ROOT) {
		IBENCH_ROOT = iBENCH_ROOT;
	}

	public String getIBENCH_MAPPING_OUT_FOLDER() {
		return IBENCH_MAPPING_OUT_FOLDER;
	}

	public void setIBENCH_MAPPING_OUT_FOLDER(String iBENCH_MAPPING_OUT_FOLDER) {
		IBENCH_MAPPING_OUT_FOLDER = iBENCH_MAPPING_OUT_FOLDER;
	}

	public String getIBENCH_MAPPING_CONFIG_FILENAME() {
		return IBENCH_MAPPING_CONFIG_FILENAME;
	}

	public void setIBENCH_MAPPING_CONFIG_FILENAME(String iBENCH_MAPPING_CONFIG_FILENAME) {
		IBENCH_MAPPING_CONFIG_FILENAME = iBENCH_MAPPING_CONFIG_FILENAME;
	}

	public String getWORK_ROOT() {
		return WORK_ROOT;
	}

	public void setWORK_ROOT(String wORK_ROOT) {
		WORK_ROOT = wORK_ROOT;
	}

	public String getUNIVERSAL_EXAMPLE_C_PATH() {
		return UNIVERSAL_EXAMPLE_C_PATH;
	}

	public void setUNIVERSAL_EXAMPLE_C_PATH(String uNIVERSAL_EXAMPLE_C_PATH) {
		UNIVERSAL_EXAMPLE_C_PATH = uNIVERSAL_EXAMPLE_C_PATH;
	}

	public String getMAPPING_SCENARIO_FILENAME() {
		return MAPPING_SCENARIO_FILENAME;
	}

	public void setMAPPING_SCENARIO_FILENAME(String mAPPING_SCENARIO_FILENAME) {
		MAPPING_SCENARIO_FILENAME = mAPPING_SCENARIO_FILENAME;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static String formattingUniversalInstance(ArrayList<Fact> chased) {
		StringBuffer sb = new StringBuffer();
		for (Fact f : chased) {
			sb.append(f);
			sb.append(" ");
		}

		return sb.toString().trim();
	}

	//
	public static String formattingUniversalInstance(HashSet<Fact> chased) {
		StringBuffer sb = new StringBuffer();
		for (Fact f : chased) {
			sb.append(f);
			sb.append(" ");
		}

		return sb.toString().trim();
	}

	public static HashSet<Fact> ChaseToGenerateC(iBenchMappingScenario scenario, Example example) {
		PostgresConnector.populateNewTableDDL(example);
		GAVmapping goal = scenario.goalMapping;
		return GAVmapping.chase(goal, example);
	}

	public ArrayList<Fact> getAllSourceFacts() {
		ArrayList<Fact> result = new ArrayList<Fact>();

		for (String key : sourceFactsByRelation.keySet()) {
			result.addAll(sourceFactsByRelation.get(key));
		}
		return result;
	}
	
	public ArrayList<Example> createComprehensiveUniversalExamples(GAVmapping goalMapping, 
			ArrayList<Example> compSourceInstances)
	{
		ArrayList<Example> randomCompUniversal = new ArrayList<Example>();

		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();

		Instance emptyTarget = new Instance(new ArrayList<Fact>());
		
		for(Example comp : compSourceInstances)
		{
			Instance randomCompSource = comp.getSource();
			Example newC = new Example();
			newC.setSource(randomCompSource);
			newC.setTarget(emptyTarget);
			postgresConnector.populateNewTableDDL(newC);
			System.out.println("chasing with following number of facts: " + newC.getSource().getFacts().size());
			HashSet<Fact> chased = GAVmapping.chase(goalMapping, newC);
			System.out.println("New universal solution has following number of facts: " + chased.size());
			ArrayList<Fact> chasedTarget = new ArrayList<Fact>(chased);
			
			Example newUniversal = new Example();
			newUniversal.setSource(randomCompSource);
			newUniversal.setTarget(new Instance(chasedTarget));
			randomCompUniversal.add(newUniversal);
//			System.out.println("Creating a new Universal Example");
//			System.out.println(newUniversal);
		}
		postgresConnector.closeUp();
		return randomCompUniversal;
	}
	
	public ArrayList<Example> createRandomUniversalExamples(GAVmapping goalMapping, 
			ArrayList<Instance> randomSourceInstances, HashMap<GavExample, GAVconstraint> triggeredMap, 
			OptimizedPostgresConnector postgresConnector)
	{
		ArrayList<Example> randomUniversal = new ArrayList<Example>();

//		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		

		Instance emptyTarget = new Instance(new ArrayList<Fact>());
		
		for(Instance randomSource : randomSourceInstances)
		{
			Example newC = new Example();
			newC.setSource(randomSource);
			newC.setTarget(emptyTarget);
			postgresConnector.populateNewTableDDL(newC);
			HashSet<Fact> chased = GAVmapping.chase(goalMapping, newC);
			ArrayList<Fact> chasedTarget = new ArrayList<Fact>(chased);
			
			Example newUniversal = new Example();
			newUniversal.setSource(randomSource);
			newUniversal.setTarget(new Instance(chasedTarget));
			randomUniversal.add(newUniversal);
			if(!hasCommon(newUniversal))
			{
				
				System.out.println("found an odd universal example = " + newUniversal);
				System.out.println("NewC = " + newC);
				System.exit(0);
			}
//			System.out.println("Creating a new Universal Example");
//			System.out.println(newUniversal);
		}
		// postgresConnector.closeUp();
		return randomUniversal;
	}
	public Example createRandomUniversalExamples_REFINED(GAVmapping goalMapping, 
			Instance randomSource, OptimizedPostgresConnector postgresConnector)
	{
//		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		

		Instance emptyTarget = new Instance(new ArrayList<Fact>());
		Example newC = new Example();
		newC.setSource(randomSource);
		newC.setTarget(emptyTarget);
		postgresConnector.populateNewTableDDL(newC);
		HashSet<Fact> chased = GAVmapping.chase(goalMapping, newC);
		ArrayList<Fact> chasedTarget = new ArrayList<Fact>(chased);
		
		Example newUniversal = new Example();
		newUniversal.setSource(randomSource);
		newUniversal.setTarget(new Instance(chasedTarget));
		return newUniversal;
		
	}
	
	public ArrayList<Example> createRandomUniversalExamples(GAVmapping goalMapping, 
			ArrayList<Instance> randomSourceInstances, OptimizedPostgresConnector postgresConnector)
	{
		ArrayList<Example> randomUniversal = new ArrayList<Example>();

//		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		

		Instance emptyTarget = new Instance(new ArrayList<Fact>());
		
		for(Instance randomSource : randomSourceInstances)
		{
			Example newC = new Example();
			newC.setSource(randomSource);
			newC.setTarget(emptyTarget);
			postgresConnector.populateNewTableDDL(newC);
			HashSet<Fact> chased = GAVmapping.chase(goalMapping, newC);
			ArrayList<Fact> chasedTarget = new ArrayList<Fact>(chased);
			
			Example newUniversal = new Example();
			newUniversal.setSource(randomSource);
			newUniversal.setTarget(new Instance(chasedTarget));
			randomUniversal.add(newUniversal);
			if(!hasCommon(newUniversal))
			{
				
				System.out.println("found an odd universal example = " + newUniversal);
				System.out.println("NewC = " + newC);
				System.exit(0);
			}
//			System.out.println("Creating a new Universal Example");
//			System.out.println(newUniversal);
		}
		// postgresConnector.closeUp();
		return randomUniversal;
	}
	
	public boolean hasNonemptyIntersection(Fact F1, Fact F2)
	{
		return Fact.hasNonemptyIntersection(F1, F2);
	}

	public boolean hasCommon(Example ex)
	{
//		refined.setTargetFact(rawCounterExample.getTargetFact());
//		Fact F = refined.getTargetFact();
		ArrayList<Fact> allSourceFacts = ex.getSource().getFacts();
		ArrayList<Fact> allTargetFacts = ex.getTarget().getFacts();

		for(Fact targetFact : allTargetFacts)
		{
			boolean foundCommon = false;
			for(Fact sourceFact : allSourceFacts)
			{
				if(hasNonemptyIntersection(targetFact, sourceFact))
				{
					foundCommon = true;
					break;
				}
			}
			if(!foundCommon)
			{
				System.out.println("weired target fact  = " + targetFact);
				return false;
			}
		}
		return true;
		
//		for(Fact f : allSourceFacts)
//		{
//			if(hasNonemptyIntersection(F,f))
//			{
//				refinedSourceFacts.add(f);
//			}
//		}
//		Instance newSource = new Instance(refinedSourceFacts);
//		refined.setSource(newSource);
//		return refined;
	}
	// an attempt to generate random universal examples from the universal
	// examples
	// generated by iBench with fixed selectivity
	public static ArrayList<Instance> createRandomSourcesWithFixedSelectivity(Example universal, int randomNbr,
			double selectivityRatio) {
		ArrayList<Instance> randomSources = new ArrayList<Instance>();
		HashSet<ArrayList<Fact>> instanceRepo = new HashSet<ArrayList<Fact>>();
		Instance inputSource = universal.getSource();
		HashMap<String, ArrayList<Fact>> factMap = inputSource.getFactMap();
		ArrayList<Fact> factPool = inputSource.getFacts();

		while (true) {
			if (randomSources.size() >= randomNbr)
				break;
			ArrayList<Fact> newRandomSource = new ArrayList<Fact>();
			for (Fact f : factPool) {
				if (isOn(selectivityRatio)) {
					newRandomSource.add(f);
				}
			}
			if (!instanceRepo.contains(newRandomSource)) {
				Instance newSource = new Instance(newRandomSource);
				instanceRepo.add(newRandomSource);
//				System.out
//						.println(String.format("created a new source instance with %s facts", newRandomSource.size()));
				randomSources.add(newSource);
			}
		}
		return randomSources;
	}
	
	public static Instance createRandomSourcesWithFixedSelectivity_REFINED(Example universal,
			double selectivityRatio) {
		ArrayList<Instance> randomSources = new ArrayList<Instance>();
		HashSet<ArrayList<Fact>> instanceRepo = new HashSet<ArrayList<Fact>>();
		Instance inputSource = universal.getSource();
		HashMap<String, ArrayList<Fact>> factMap = inputSource.getFactMap();
		ArrayList<Fact> factPool = inputSource.getFacts();
		ArrayList<Fact> newRandomSource = new ArrayList<Fact>();
		for (Fact f : factPool) {
			if (isOn(selectivityRatio)) {
				newRandomSource.add(f);
			}
		}
		Instance newSource = new Instance(newRandomSource);
		instanceRepo.add(newRandomSource);
		return newSource;
	}

	public static ArrayList<Instance> 
		createRandomSourcesWithRandomSelectivity(Example universal, int randomNbr) {
		ArrayList<Instance> randomSources = new ArrayList<Instance>();
		HashSet<ArrayList<Fact>> instanceRepo = new HashSet<ArrayList<Fact>>();
		Instance inputSource = universal.getSource();
		HashMap<String, ArrayList<Fact>> factMap = inputSource.getFactMap();
		ArrayList<Fact> factPool = inputSource.getFacts();

		while (true) {
			if (randomSources.size() >= randomNbr)
				break;
			ArrayList<Fact> newRandomSource = new ArrayList<Fact>();
			double selectivityRatio = Math.random();
			for (Fact f : factPool) {
				if (isOn(selectivityRatio)) {
					newRandomSource.add(f);
				}
			}
			if (!instanceRepo.contains(newRandomSource)) {
				Instance newSource = new Instance(newRandomSource);
				instanceRepo.add(newRandomSource);
				System.out
						.println(String.format("created a new source instance with %s facts", newRandomSource.size()));
				randomSources.add(newSource);
			}
		}
		return randomSources;
	}
	
	private static double getRandomSelectivityWithRange(double lowerbound)
	{
		double r = Math.random();
		if(r < lowerbound)
			return lowerbound;
		else
			return r;
	}
	public static ArrayList<Instance> 
		createRandomSources_RandomSelectivity_WithLowerBound(Example universal, int randomNbr, double lowerbound) {
	ArrayList<Instance> randomSources = new ArrayList<Instance>();
	HashSet<ArrayList<Fact>> instanceRepo = new HashSet<ArrayList<Fact>>();
	Instance inputSource = universal.getSource();
	HashMap<String, ArrayList<Fact>> factMap = inputSource.getFactMap();
	ArrayList<Fact> factPool = inputSource.getFacts();

	while (true) {
		if (randomSources.size() >= randomNbr)
			break;
		ArrayList<Fact> newRandomSource = new ArrayList<Fact>();
		double selectivityRatio = getRandomSelectivityWithRange(lowerbound);
		for (Fact f : factPool) {
			if (isOn(selectivityRatio)) {
				newRandomSource.add(f);
			}
		}
		if (!instanceRepo.contains(newRandomSource)) {
			Instance newSource = new Instance(newRandomSource);
			instanceRepo.add(newRandomSource);
//			System.out
//					.println(String.format("\ncreated a new source instance with %s facts", newRandomSource.size()));
//			System.out.println(newRandomSource);
			randomSources.add(newSource);
		}
	}
	return randomSources;
}


	// a helper function for createRandomExamples()
	private static boolean isOn(double selectivityRatio) {
		double reverseSelectivityRatio = 1.0 - selectivityRatio;
		double r = Math.random();
		return r >= reverseSelectivityRatio;
	}

	public static Example createExample(iBenchMappingScenario scenario) {
		Example newE = new Example();
		Instance sourceIns = new Instance(scenario.getAllSourceFacts());
		Instance targetIns = new Instance();
		newE.setSource(sourceIns);
		newE.setTarget(targetIns);

		return newE;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("\n************ Parsed Mapping Scneario *************\n");
		sb.append("sourceSchema:\n{");
		sb.append(sourceSchema + "}\n");

		sb.append("targetSchema:\n{");
		sb.append(targetSchema + "}\n");

		sb.append("goalMapping:\n{\n");
		sb.append(goalMapping + "}\n");
		
		sb.append("Parameter Settings:\n{\n");
//		System.out.println("CHecking parameters" + parameterSettings);
		sb.append(parameterSettings + "}\n");

		sb.append("goalMapping_readable:\n{\n");
		sb.append(goalMapping.toReadableString() + "}\n");

		sb.append("sourceFactsByRelation:\n{\n");
		for (String rel : sourceFactsByRelation.keySet()) {
			sb.append("\t" + rel + ": " + sourceFactsByRelation.get(rel) + "\n");
		}
		sb.append("\n}\n");
		sb.append("**************************************************");

		return sb.toString();
	}

	public void outputScenario() {
		String scenarioOutPath = null;
		if (WORK_ROOT.endsWith("/")) {
			scenarioOutPath = WORK_ROOT + MAPPING_SCENARIO_FILENAME;
		} else {
			scenarioOutPath = WORK_ROOT + "/" + MAPPING_SCENARIO_FILENAME;
		}
		try {
			FileWriter outWriter = new FileWriter(scenarioOutPath);
//			outWriter.write(toString());
			outWriter.write(toString());
			outWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void outputScenario_REFINED(String scenarioName) {
		String scenarioOutPath = null;
		if (WORK_ROOT.endsWith("/")) {
			scenarioOutPath = WORK_ROOT + Keywords.MAPPING_SCENARIO_FOLDER+"/"+ scenarioName;
		} else {
			scenarioOutPath = WORK_ROOT + Keywords.MAPPING_SCENARIO_FOLDER+"/"+ scenarioName;
		}
		try {
			FileWriter outWriter = new FileWriter(scenarioOutPath);
//			outWriter.write(toString());
			outWriter.write(toString());
			outWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void outputScenario(String fileout) {
		try {
			FileWriter outWriter = new FileWriter(fileout);
			outWriter.write(toString());
			outWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setPropertiesObject(String pathToProperties) {
		try {
			FileInputStream in = new FileInputStream(pathToProperties);
			config.load(in);
			IBENCH_ROOT = config.getProperty(Keywords.IBENCH_ROOT);
			IBENCH_MAPPING_OUT_FOLDER = config.getProperty(Keywords.IBENCH_MAPPING_OUT_FOLDER);
			IBENCH_MAPPING_CONFIG_FILENAME = config.getProperty(Keywords.IBENCH_MAPPING_CONFIG_FILENAME);
			WORK_ROOT = config.getProperty(Keywords.WORK_ROOT);
			UNIVERSAL_EXAMPLE_C_PATH = config.getProperty(Keywords.UNIVERSAL_EXAMPLE_C_PATH);
			MAPPING_SCENARIO_FILENAME = config.getProperty(Keywords.MAPPING_SCENARIO_FILENAME);
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public iBenchMappingScenario() {
		sourceFactsByRelation = new HashMap<String, ArrayList<Fact>>();
	}

	public void addParsedFacts(String relationSymbol, ArrayList<Fact> facts) {
		if (!sourceFactsByRelation.containsKey(relationSymbol)) {
			sourceFactsByRelation.put(relationSymbol, facts);
		} else {
			ArrayList<Fact> previousFacts = sourceFactsByRelation.get(relationSymbol);
			previousFacts.addAll(facts);
			sourceFactsByRelation.put(relationSymbol, previousFacts);
		}
	}

	public GAVmapping getGoalMapping() {
		return goalMapping;
	}

	public void setGoalMapping(GAVmapping goalMapping) {
		this.goalMapping = goalMapping;
	}

	public Schema getSourceSchema() {
		return sourceSchema;
	}

	public void setSourceSchema(Schema sourceSchema) {
		this.sourceSchema = sourceSchema;
	}

	public Schema getTargetSchema() {
		return targetSchema;
	}

	public void setTargetSchema(Schema targetSchema) {
		this.targetSchema = targetSchema;
	}

	public HashMap<String, ArrayList<Fact>> getSourceFactsByRelation() {
		return sourceFactsByRelation;
	}

	public void setSourceFactsByRelation(HashMap<String, ArrayList<Fact>> sourceFactsByRelation) {
		this.sourceFactsByRelation = sourceFactsByRelation;
	}

}
