package com.learning.ibench.adapters;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.learning.example.Fact;
import com.learning.example.GavExample;
import com.learning.example.Instance;
import com.learning.learn.Keywords;
import com.learning.mapping.*;

public class iBenchMappingParser {
	
	String IBENCH_ROOT = null;
	String IBENCH_MAPPING_OUT_FOLDER = null;
	String IBENCH_MAPPING_CONFIG_FILENAME = null;
	String IBENCH_CONFIGFILE_TEMPLATE_PATH = null;
	String IBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH = null;

	
	public static boolean isMappingFile(String fileName)
	{
		if(fileName.startsWith(Keywords.OUTFILE_SCHEMAS_PREFIX) && fileName.endsWith(Keywords.XML_EXTENSION))
			return true;
		return false;
	}
	public static boolean isInstanceFile(String fileName)
	{
		if(fileName.endsWith(Keywords.CSV_EXTENSION))
			return true;
		return false;
	}
	
	boolean isComprehensive = false;
	boolean isRandom 		= false;
	public void TriggerComprehensive(boolean val)
	{
		isComprehensive = val;
	}
	public void TriggerRandom(boolean val)
	{
		isRandom = val;
	}
	
	public iBenchMappingScenario parse_REFINED(String outFolderName)
	{
		iBenchMappingScenario mappingScenario = new iBenchMappingScenario();
		String pathToiBenchOutputDir = null;
		if(isComprehensive)
		{
			// learning mode is to learn from comprehensive examples
			pathToiBenchOutputDir = IBENCH_ROOT+ "/" + Keywords.COMPREHENSIVE_SCENARIOS_FOLDER+"/"+outFolderName;
//			System.out.println("pathToiBenchOutputDir="+pathToiBenchOutputDir);
		}
		if(isRandom)
		{
			pathToiBenchOutputDir = IBENCH_ROOT+ "/" + Keywords.RANDOM_SCENARIOS_FOLDER+"/"+outFolderName;

		}
		else
		{
			if(IBENCH_ROOT.endsWith("/")){
				pathToiBenchOutputDir = IBENCH_ROOT+outFolderName ;
			}
			else
			{
				pathToiBenchOutputDir = IBENCH_ROOT+"/"+outFolderName;
			}
		}		
			
		System.out.println("PATH= " + pathToiBenchOutputDir);
		File workDIR = new File(pathToiBenchOutputDir);
		File[] allFiles = workDIR.listFiles();
		for(int i = 0; i < allFiles.length; i++)
		{
			if((allFiles[i].isFile()))
			{
				if(isMappingFile(allFiles[i].getName()))
				{
//					System.out.println("Mapping File = "+ allFiles[i].getName());
					Schema sourceSchema = iBenchMappingParser.parseSourceSchema(allFiles[i].getAbsolutePath());
					Schema targetSchema = iBenchMappingParser.parseTargetSchema(allFiles[i].getAbsolutePath());
					GAVmapping goalMapping = iBenchMappingParser.parseMappings(allFiles[i].getAbsolutePath());
					mappingScenario.setGoalMapping(goalMapping);
					mappingScenario.setSourceSchema(sourceSchema);
					mappingScenario.setTargetSchema(targetSchema);
				}
				else
				{
					if(isInstanceFile(allFiles[i].getName()))
					{
//						System.out.println("Instance File = "+ allFiles[i].getName());
						ArrayList<Fact> parsed = parseCsvToFacts(allFiles[i]);
						mappingScenario.addParsedFacts(removeFileExtenstion(allFiles[i].getName()), parsed);
					}
				}
			}
		}
		return mappingScenario;
	}
	
	public iBenchMappingScenario parse()
	{
		iBenchMappingScenario mappingScenario = new iBenchMappingScenario();
		String pathToiBenchOutputDir = null;
		if(isComprehensive)
		{
			// learning mode is to learn from comprehensive examples
			pathToiBenchOutputDir = IBENCH_ROOT+ "/" + Keywords.COMPREHENSIVE_SCENARIOS_FOLDER+"/"+IBENCH_MAPPING_OUT_FOLDER;
//			System.out.println("pathToiBenchOutputDir="+pathToiBenchOutputDir);
		}
		if(isRandom)
		{
			pathToiBenchOutputDir = IBENCH_ROOT+ "/" + Keywords.RANDOM_SCENARIOS_FOLDER+"/"+IBENCH_MAPPING_OUT_FOLDER;

		}
		else
		{
			if(IBENCH_ROOT.endsWith("/")){
				pathToiBenchOutputDir = IBENCH_ROOT+IBENCH_MAPPING_OUT_FOLDER ;
			}
			else
			{
				pathToiBenchOutputDir = IBENCH_ROOT+"/"+IBENCH_MAPPING_OUT_FOLDER;
			}
		}		
			
		System.out.println("PATH= " + pathToiBenchOutputDir);
		File workDIR = new File(pathToiBenchOutputDir);
		File[] allFiles = workDIR.listFiles();
		for(int i = 0; i < allFiles.length; i++)
		{
			if((allFiles[i].isFile()))
			{
				if(isMappingFile(allFiles[i].getName()))
				{
//					System.out.println("Mapping File = "+ allFiles[i].getName());
					Schema sourceSchema = iBenchMappingParser.parseSourceSchema(allFiles[i].getAbsolutePath());
					Schema targetSchema = iBenchMappingParser.parseTargetSchema(allFiles[i].getAbsolutePath());
					GAVmapping goalMapping = iBenchMappingParser.parseMappings(allFiles[i].getAbsolutePath());
					mappingScenario.setGoalMapping(goalMapping);
					mappingScenario.setSourceSchema(sourceSchema);
					mappingScenario.setTargetSchema(targetSchema);
				}
				else
				{
					if(isInstanceFile(allFiles[i].getName()))
					{
//						System.out.println("Instance File = "+ allFiles[i].getName());
						ArrayList<Fact> parsed = parseCsvToFacts(allFiles[i]);
						mappingScenario.addParsedFacts(removeFileExtenstion(allFiles[i].getName()), parsed);
					}
				}
			}
		}
		return mappingScenario;
	}
	
	public Properties getConfig() {
		return config;
	}
	public void setConfig(Properties config) {
		this.config = config;
	}
	
	String IBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED;

	public void setIBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED(String iBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED) {
		IBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED = iBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED;
	}

	Properties config = new Properties();
	public iBenchMappingParser(String fileToProperties)
	{
		try 
		{
			FileInputStream in = new FileInputStream(fileToProperties);
			config.load(in);
			IBENCH_ROOT 					= config.getProperty(Keywords.IBENCH_ROOT);
			IBENCH_MAPPING_OUT_FOLDER		= config.getProperty(Keywords.IBENCH_MAPPING_OUT_FOLDER);
			IBENCH_MAPPING_CONFIG_FILENAME 	= config.getProperty(Keywords.IBENCH_MAPPING_CONFIG_FILENAME);
			IBENCH_CONFIGFILE_TEMPLATE_PATH	= (config.getProperty(Keywords.IBENCH_ROOT).endsWith("/") ? 
					config.getProperty(Keywords.IBENCH_ROOT)+Keywords.CONFIGFILE_TEMPLATE_FILENAME : 
						config.getProperty(Keywords.IBENCH_ROOT)+ "/"+Keywords.CONFIGFILE_TEMPLATE_FILENAME	);
			IBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH = (config.getProperty(Keywords.IBENCH_ROOT).endsWith("/") ? 
					config.getProperty(Keywords.IBENCH_ROOT)+Keywords.CONFIGFILE_TEMPLATE_FILENAME_Random : 
						config.getProperty(Keywords.IBENCH_ROOT)+ "/"+Keywords.CONFIGFILE_TEMPLATE_FILENAME_Random	);
			IBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED = (config.getProperty(Keywords.IBENCH_ROOT).endsWith("/") ? 
					config.getProperty(Keywords.IBENCH_ROOT)+Keywords.CONFIGFILE_TEMPLATE_FILENAME_Random_REFINED : 
						config.getProperty(Keywords.IBENCH_ROOT)+ "/"+Keywords.CONFIGFILE_TEMPLATE_FILENAME_Random_REFINED	);
			in.close();
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getIBENCH_ROOT() {
		return IBENCH_ROOT;
	}
	public void setIBENCH_ROOT(String iBENCH_ROOT) {
		IBENCH_ROOT = iBENCH_ROOT;
	}
	public String getIBENCH_MAPPING_OUT_FOLDER() {
		return IBENCH_MAPPING_OUT_FOLDER;
	}
	public void setIBENCH_MAPPING_OUT_FOLDER(String iBENCH_MAPPING_OUT_FOLDER) {
		IBENCH_MAPPING_OUT_FOLDER = iBENCH_MAPPING_OUT_FOLDER;
	}
	public String getIBENCH_MAPPING_CONFIG_FILENAME() {
		return IBENCH_MAPPING_CONFIG_FILENAME;
	}
	public void setIBENCH_MAPPING_CONFIG_FILENAME(String iBENCH_MAPPING_CONFIG_FILENAME) {
		IBENCH_MAPPING_CONFIG_FILENAME = iBENCH_MAPPING_CONFIG_FILENAME;
	}
	public String getIBENCH_CONFIGFILE_TEMPLATE_PATH() {
		return IBENCH_CONFIGFILE_TEMPLATE_PATH;
	}
	
	public String getIBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH() {
		return IBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH;
	}
	public String getIBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED() {
		return IBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED;
	}
	
	public void setIBENCH_CONFIGFILE_TEMPLATE_PATH(String iBENCH_CONFIGFILE_TEMPLATE_PATH) {
		IBENCH_CONFIGFILE_TEMPLATE_PATH = iBENCH_CONFIGFILE_TEMPLATE_PATH;
	}
	/**
	 * 
	 * @param pathToiBenchOutputDirn = the directory which is the output of iBench
	 */
	public static iBenchMappingScenario parse(String pathToiBenchOutputDir)
	{
		iBenchMappingScenario mappingScenario = new iBenchMappingScenario();
		File workDIR = new File(pathToiBenchOutputDir);
		File[] allFiles = workDIR.listFiles();
		for(int i = 0; i < allFiles.length; i++)
		{
			if((allFiles[i].isFile()))
			{
				if(isMappingFile(allFiles[i].getName()))
				{
//					System.out.println("Mapping File = "+ allFiles[i].getName());
					Schema sourceSchema = iBenchMappingParser.parseSourceSchema(allFiles[i].getAbsolutePath());
					Schema targetSchema = iBenchMappingParser.parseTargetSchema(allFiles[i].getAbsolutePath());
					GAVmapping goalMapping = iBenchMappingParser.parseMappings(allFiles[i].getAbsolutePath());
					mappingScenario.setGoalMapping(goalMapping);
					mappingScenario.setSourceSchema(sourceSchema);
					mappingScenario.setTargetSchema(targetSchema);
				}
				else
				{
					if(isInstanceFile(allFiles[i].getName()))
					{
//						System.out.println("Instance File = "+ allFiles[i].getName());
						ArrayList<Fact> parsed = parseCsvToFacts(allFiles[i]);
						mappingScenario.addParsedFacts(removeFileExtenstion(allFiles[i].getName()), parsed);
					}
				}
			}
		}
		System.out.println("Mapping scenario is successfully parsed");
		return mappingScenario;
	}
	
	
	// the input csv file is of the form: 1|val1|val2|val3
	// it turned out that the first number "1" is redundant, and should not be added to the fact
	public static ArrayList<String> lineToTokens(String line, char delimiter)
	{
		StringBuffer sb = new StringBuffer();
		ArrayList<String> tokens = new ArrayList<String>();
		int counter = 0;
		
		char[] str = line.toCharArray();
		for(char c : str)
		{
			if(c!=delimiter)
			{
				sb.append(c);
			}
			else
			{
				// current char is the delimiter
				if(counter==0)
				{
					sb = new StringBuffer();
				}
				else
				{
					tokens.add(sb.toString());
					sb = new StringBuffer();
				}
				counter++;
			}
		}
		if(sb.length()>0)
		{
			tokens.add(sb.toString());
		}
		return tokens;
	}
	
	public static String removeFileExtenstion(String str)
	{
		int index = str.lastIndexOf(".");
		return str.substring(0, index);
	}
	
	public static ArrayList<Fact> parseCsvToFacts(File csvFile)
	{
		ArrayList<Fact> parsedFacts = new ArrayList<Fact>();
		try 
		{
			String relationSymbol = removeFileExtenstion(csvFile.getName());
			FileInputStream fis = new FileInputStream(csvFile);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while((line=br.readLine())!=null)
			{
//				System.out.println("Processing line = "+ line);
//				System.out.println(line.contains("|"));
//				String[] values = line.split("|");
//				System.out.println(values.length);
//				printStringArray(values);
				ArrayList<String> values = lineToTokens(line, '|');
//				System.out.println(relationSymbol+ values);
				Fact newF = Fact.createFact(relationSymbol, values);
				parsedFacts.add(newF);
			}
		} 
		catch (FileNotFoundException e) {e.printStackTrace();} 
		catch (IOException e) {e.printStackTrace();}
		
		return parsedFacts;
	}
	public static void printStringArray (String[] A)
	{
		for(String s : A)
		{
			System.out.print(s+"<->");
		}
		System.out.println();
	}
	
	public static Schema parseSourceSchema(String pathToMappingXML)
	{
		Schema newSourceSchema = new Schema();
		try {
			File inputFile = new File(pathToMappingXML);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			XPath xPath = XPathFactory.newInstance().newXPath();
			
			int numOfRelations = 0;

			String pathToSourceSchema = Keywords.XML_PATH_SOURCESCHEMA+"/"+Keywords.XML_RELATION_NODE;
//			System.out.println(doc.getDocumentElement().getNodeName());
			NodeList sourceRelationList = (NodeList) xPath.compile(pathToSourceSchema).evaluate(doc, XPathConstants.NODESET);
			numOfRelations = sourceRelationList.getLength();
			for (int i = 0; i < sourceRelationList.getLength(); i++) {
	            Node currentSrcRelation = sourceRelationList.item(i);
	            Relation newSourceRelation = new Relation();
	            newSourceRelation.setRelationSymbol( getRelationName(currentSrcRelation));
	            newSourceRelation.setAttributes(getAttrList(currentSrcRelation));
	            newSourceSchema.addRelation(newSourceRelation);
			}
		} 
		catch (Exception e) {e.printStackTrace();}
//		System.out.println("Parsed "+ newSourceSchema.getRelations().size()+" source relations");
		return newSourceSchema;
	}
	
	public static Schema parseTargetSchema(String pathToMappingXML)
	{
		Schema newTargetSchema = new Schema();
		try {
			File inputFile = new File(pathToMappingXML);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			XPath xPath = XPathFactory.newInstance().newXPath();
			
			int numOfRelations = 0;

			String pathToTargetSchema = Keywords.XML_PATH_TARGETSCHEMA+"/"+Keywords.XML_RELATION_NODE;
//			System.out.println(doc.getDocumentElement().getNodeName());
			NodeList targetRelationList = (NodeList) xPath.compile(pathToTargetSchema).evaluate(doc, XPathConstants.NODESET);
			numOfRelations = targetRelationList.getLength();
			for (int i = 0; i < targetRelationList.getLength(); i++) {
	            Node currentTrgRelation = targetRelationList.item(i);
	            Relation newTargetRelation = new Relation();
	            newTargetRelation.setRelationSymbol( getRelationName(currentTrgRelation));
	            newTargetRelation.setAttributes(getAttrList(currentTrgRelation));
	            newTargetSchema.addRelation(newTargetRelation);
			}
		} 
		catch (Exception e) {e.printStackTrace();}
//		System.out.println("Parsed "+ newTargetSchema.getRelations().size()+" target relations");
		return newTargetSchema;
	}
	
	
	public static GAVmapping parseMappings(String pathToMappingXML)
	{
		GAVmapping mapping = new GAVmapping();
		try {
			File inputFile = new File(pathToMappingXML);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			XPath xPath = XPathFactory.newInstance().newXPath();
			
			int numOfMappings = 0;

			String pathToMappings = Keywords.XML_PATH_TO_MAPPINGS; // goes to "/MappingScenario/Mappings"
						
			NodeList mappingList = (NodeList) xPath.compile(pathToMappings).evaluate(doc, XPathConstants.NODESET);
			numOfMappings = mappingList.getLength();
			for (int i = 0; i < mappingList.getLength(); i++) {
	            Node currentMapping = mappingList.item(i);
	            // a new GAV mapping
//	            System.out.println("parsing mapping "+ getMappingId(currentMapping));
	            GavExample exampleAsConstraint = parseConstraint(currentMapping);
	            mapping.addConstraint(exampleAsConstraint);
			}
		} 
		catch (Exception e) {e.printStackTrace();}
		return mapping;
	}
	
	public static GavExample parseConstraint(Node currMapping)
	{
		GavExample gav = new GavExample();
		Instance newSource = new Instance();
		Fact targetFact = new Fact();
				
		NodeList attrs = currMapping.getChildNodes();
		ArrayList<Fact> canonicalSourceFacts = new ArrayList<Fact>();
		
		// create a list, but in fact, in the GAV
		// case, it must contain only one fact
		ArrayList<Fact> canonicalTargetFacts = new ArrayList<Fact>(); 
		
		
		for(int i = 0; i < attrs.getLength(); i++)
		{
			// for each <Attr> node, get its value
			Node constraintNode = attrs.item(i); // constraintNode = <Foreach> or <Exists>
			if(constraintNode.getNodeName().equals(Keywords.XML_MAPPING_FOREACH_NODE))
			{
				for(int k = 0; k <constraintNode.getChildNodes().getLength(); k++)
				{
					Node curr = constraintNode.getChildNodes().item(k);
					if(curr.getNodeName().equals(Keywords.XML_MAPPING_ATOM_NODE))
					{
						Fact newF = new Fact();
						newF.setRelationSymbol(getAtomRelationSymbol(curr));
						newF.setTuples(getVars(curr));
						canonicalSourceFacts.add(newF);
					}
				}
				// a new atom A, i.e., a new canonical fact over A
			}
			if(constraintNode.getNodeName().equals(Keywords.XML_MAPPING_EXISTS_NODE))
			{
				// right-hand side - a single target fact
				for(int k = 0; k <constraintNode.getChildNodes().getLength(); k++)
				{
					Node curr = constraintNode.getChildNodes().item(k);
					if(curr.getNodeName().equals(Keywords.XML_MAPPING_ATOM_NODE))
					{
						Fact newF = new Fact();
						newF.setRelationSymbol(getAtomRelationSymbol(curr));
						newF.setTuples(getVars(curr));
						canonicalTargetFacts.add(newF);
					}
				}
			}
		}
		newSource.setFacts(canonicalSourceFacts);
		gav.setSource(newSource);
		targetFact = canonicalTargetFacts.get(0);
		gav.setTargetFact(targetFact);
//		System.out.println("A new GAV mapping (example)="+ gav);
		return gav;
	}
	public static ArrayList<String> getVars(Node node)
	{
		ArrayList<String> list = new ArrayList<String>();
		NodeList varList = node.getChildNodes();

		for(int i = 0; i < varList.getLength(); i++)
		{
			// for each <Attr> node, get its value
			Node n = varList.item(i);
			if(n.getNodeName().equals(Keywords.XML_MAPPING_ATOM_VAR))
			{
				list.add(n.getTextContent());
			}
		}
		return list;
	}
	
	
	public static String getAtomRelationSymbol(Node atom)
	{
		return atom.getAttributes().getNamedItem(Keywords.XML_ATOM_TABLEREF_ATTR).getNodeValue();
	}
	public static String getMappingId(Node currentMapping)
	{
		return currentMapping.getAttributes().getNamedItem(Keywords.XML_MAPPING_ID_ATTR).getNodeValue();
	}
	
	public static ArrayList<String> getAttrList(Node relationNode)
	{
		ArrayList<String> list = new ArrayList<String>();
		
		NodeList attrs = relationNode.getChildNodes();

		for(int i = 0; i < attrs.getLength(); i++)
		{
			// for each <Attr> node, get its value
			Node attrNode = attrs.item(i);
			if(attrNode.getNodeName().equals(Keywords.XML_RELATION_ATTR_NODE))
			{
				list.add(getAttrName(attrNode));
			}
		}
		return list;
	}
	
	public static String getAttrName(Node attrNode)
	{
		Element eElement = (Element) attrNode;
		return eElement.getElementsByTagName(Keywords.XML_COLUMN_NAME_NODE).item(0).getTextContent();
	}
	
	public static String getRelationName(Node currentSrcRelation)
	{
		return currentSrcRelation.getAttributes().getNamedItem(Keywords.XML_NAME_ATTRIBUTE).getNodeValue();
	}

}
