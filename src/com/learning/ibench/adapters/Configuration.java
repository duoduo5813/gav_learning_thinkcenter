package com.learning.ibench.adapters;

public class Configuration {

	// the values below corresponds to the maximum value
	static int numOfCopy 			= 10;
	static int numOfMerging 		= 10;
	static int numSelfJoin			= 0;
	static int numbOfProjection 	= 10;
	
	static long randomSeed 			= 123456789l;
	static int joinSize				= 10;
	static int numFactPerRelation	= 10;
	
	static int ReuseSourcePerc		= 100;
	static int ReuseTargetPerc		= 100;
	
	final static int ReuseSourceLowerbound = 75;
	final static int ReuseTargetLowerbound = 80;
	
	public static int getNumOfCopy() {
		return numOfCopy;
	}
	public static void setNumOfCopy(int numOfCopy) {
		Configuration.numOfCopy = numOfCopy;
	}
	public static int getNumOfMerging() {
		return numOfMerging;
	}
	public static void setNumOfMerging(int numOfMerging) {
		Configuration.numOfMerging = numOfMerging;
	}
	public static int getNumbOfProjection() {
		return numbOfProjection;
	}
	public static void setNumbOfProjection(int numbOfProjection) {
		Configuration.numbOfProjection = numbOfProjection;
	}
	public static long getRandomSeed() {
		return randomSeed;
	}
	public static void setRandomSeed(long randomSeed) {
		Configuration.randomSeed = randomSeed;
	}
	public static int getJoinSize() {
		return joinSize;
	}
	public static void setJoinSize(int joinSize) {
		Configuration.joinSize = joinSize;
	}
	public static int getNumFactPerRelation() {
		return numFactPerRelation;
	}
	public static void setNumFactPerRelation(int numFactPerRelation) {
		Configuration.numFactPerRelation = numFactPerRelation;
	}
	public static int getReuseSourcePerc() {
		return ReuseSourcePerc;
	}
	public static void setReuseSourcePerc(int reuseSourcePerc) {
		ReuseSourcePerc = reuseSourcePerc;
	}
	public static int getReuseTargetPerc() {
		return ReuseTargetPerc;
	}
	public static void setReuseTargetPerc(int reuseTargetPerc) {
		ReuseTargetPerc = reuseTargetPerc;
	}
	
	
	// # copy, merging, projection, randomSeed, joinSize(1-10), 
			// # repElementCount(max. 5), ReuseSourcePerc, ReuseTargetPerc

}
