package com.learning.ibench.adapters;

public class testConfigMain {

	public static void main(String[] args)
	{
		String pathToProperties = "/Users/kunqian/git/gav_learning/GAVLearning/learningConfig";
		iBenchMappingParser iBenchParser = new iBenchMappingParser(pathToProperties);
		
		iBenchMappingScenario scenario = iBenchParser.parse();
		scenario.setPropertiesObject(pathToProperties);
		
		String configOutPath = "/Users/kunqian/git/gav_learning/GAVLearning/testConfig";
		scenario.buildConfigFileForiBench(iBenchParser.getIBENCH_CONFIGFILE_TEMPLATE_PATH(), configOutPath,0);			
	}
}
