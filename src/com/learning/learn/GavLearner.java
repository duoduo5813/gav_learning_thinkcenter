package com.learning.learn;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.learning.example.*;
import com.learning.mapping.GAVmapping;

public class GavLearner {
	
	ArrayList<Example> C ;
	ArrayList<Example> checked = new ArrayList<Example>();
	GAVmapping candidate;
	GAVmapping goalMapping;
	OptimizedPostgresConnector connector = null;
	
	public void setOptimizedPOstgresConnector(OptimizedPostgresConnector c)
	{
		connector = c;
	}
	
	final int maxIteration = 50;
	
	public GavLearner()
	{
		checked = new ArrayList<Example>();
		candidate = new GAVmapping();
		goalMapping = new GAVmapping();
	}
	

	
	public GavLearner(String pathToUniversalExample, GAVmapping goal)
	{
		connector.resetGavLearningDB();
		C = Example.BatchLoad(pathToUniversalExample);
		goalMapping = goal;
		candidate = new GAVmapping();
	}
	
	// learning parameters
	Properties config = new Properties();
	int NUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES;
	int TESTSET_PERCENTAGE;
	int MAX_LEARNING_ITERATION;
	double RANDOM_SELECTIVITY_LOWERBOUND;
	int numOfIterationsPerformed = 0;
	public GavLearner(String pathToProperties)
	{
		try 
		{
			FileInputStream in = new FileInputStream(pathToProperties);
			config.load(in);
			NUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES	= Integer.parseInt(config.getProperty(Keywords.NUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES));
			TESTSET_PERCENTAGE					= Integer.parseInt(config.getProperty(Keywords.TESTSET_PERCENTAGE));
			MAX_LEARNING_ITERATION				= Integer.parseInt(config.getProperty(Keywords.MAX_LEARNING_ITERATION));
			RANDOM_SELECTIVITY_LOWERBOUND		= Double.parseDouble(config.getProperty(Keywords.RANDOM_SELECTIVITY_LOWERBOUND));
			in.close();
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		checked = new ArrayList<Example>();
		candidate = new GAVmapping();
		goalMapping = new GAVmapping();
	}
	
	public void setPathToUniversalExamples(String path)
	{
		C = Example.BatchLoad(path);
	}
	public void setGoalMapping(GAVmapping goal)
	{
		goalMapping = goal;
	}
	
	public GavLearner(String pathToUniversalExample, String pathToGavExamplesAsGoalMapping)
	{
		PostgresConnector.resetGavLearningDB();
		C = Example.BatchLoad(pathToUniversalExample);
		goalMapping = GavExample.LoadGoalMapping(pathToGavExamplesAsGoalMapping);
		checked = new ArrayList<Example>();
		candidate = new GAVmapping();
	}
	
	public void initialization()
	{
		int count = 0;
		for(Example e : C)
		{
//			System.out.println(e);
			PostgresConnector.populateNewTableDDL(e);
			count++;
		}
		int numOfIterationsPerformed = 0;
		System.out.println(count+" universal examples have been loaded!");
	}
	
	public Properties getConfig() {
		return config;
	}



	public void setConfig(Properties config) {
		this.config = config;
	}



	public int getNUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES() {
		return NUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES;
	}



	public void setNUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES(int nUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES) {
		NUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES = nUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES;
	}



	public int getTESTSET_PERCENTAGE() {
		return TESTSET_PERCENTAGE;
	}



	public void setTESTSET_PERCENTAGE(int tESTSET_PERCENTAGE) {
		TESTSET_PERCENTAGE = tESTSET_PERCENTAGE;
	}



	public int getMAX_LEARNING_ITERATION() {
		return MAX_LEARNING_ITERATION;
	}



	public void setMAX_LEARNING_ITERATION(int mAX_LEARNING_ITERATION) {
		MAX_LEARNING_ITERATION = mAX_LEARNING_ITERATION;
	}



	public double getRANDOM_SELECTIVITY_LOWERBOUND() {
		return RANDOM_SELECTIVITY_LOWERBOUND;
	}



	public void setRANDOM_SELECTIVITY_LOWERBOUND(double rANDOM_SELECTIVITY_LOWERBOUND) {
		RANDOM_SELECTIVITY_LOWERBOUND = rANDOM_SELECTIVITY_LOWERBOUND;
	}


	int maxIterations = 1000;
	public void setMaxIteration(int max)
	{
		maxIterations = max;
	}
	
	public int getTotalNumberLearningIteration()
	{
		return numOfIterationsPerformed;
	}

	public GAVmapping learn()
	{
		int iteration = 0;
		GavExample counterexample = null;
//		System.out.println("*************"+ (iteration+1) +"-th iteration*************");
		
//		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
//		postgresConnector.initialization();
		
		GAVmapping previousCandid = new GAVmapping();
		
		while((counterexample=isC_Equivalent())!=null)
		{			
			numOfIterationsPerformed++;
			if(iteration%50 == 0)
			{
				System.out.print(iteration+ " ");
			}
			if(iteration>=maxIterations)
			{
				System.out.println("exceed the maximum learning iterations " + iteration);
				break;
			}
//			System.out.println("Candidate Mapping is not C-equivalent to the goal Mapping");
//			System.out.println("A counterexample has been returned = "+ counterexample);
			// let T = counterexample which is a GAV example
			// compute T' from T
			boolean hasImplicant = false;
			
			if(!hasImplicant)
			{
				// T' = counterexample
//				HashSet<GavExample> simplifiedConstraints = computeCriticalConstraints(counterexample, goalMapping);
				HashSet<GavExample> simplifiedConstraints = computeOneCriticalConstraints(counterexample, goalMapping, this.connector);

				
//				System.out.println("simplified constraints = "+ simplifiedConstraints);
				for(GavExample exampleAsConstraint : simplifiedConstraints)
				{
					exampleAsConstraint.setDatasourceTag("c");
					candidate.addConstraint(exampleAsConstraint);
				}
//				candidate.addConstraint(counterexample);
			}
			else
			{
				// to be constructed
			}
			
//			System.out.println("candidate==null? " + candidate==null);
			// compute critically sound constraint from T'
//			System.out.println("Now the candidate mapping is \n"+candidate);
			iteration++;
//			System.out.println("\n\n*************"+ (iteration+1) +"-th iteration*************");
			if(previousCandid.equals(candidate))
			{
				// got the same candidate mapping
				System.out.println("SAME CANDIDATE MAPPING!!\n"+ candidate);
			}
			previousCandid = candidate.makeCopy();
		}
		
//		postgresConnector.closeUp();
//		System.out.println("\nLearned GAV= \n"+candidate);
		return candidate;
	}
	
	// a blocking funcion that first eliminates atoms that will obviously not be used
	// in the final result. Concretely, if the intersection of the active domain of an atom A and
	// the active domain of F is empty, then A will never be picked 
	public GavExample blocking(GavExample rawCounterExample)
	{
		GavExample refined = new GavExample();
		refined.setTargetFact(rawCounterExample.getTargetFact());
		Fact F = refined.getTargetFact();
		ArrayList<Fact> allSourceFacts = rawCounterExample.getSource().getFacts();
		ArrayList<Fact> refinedSourceFacts = new ArrayList<Fact>();
		
		for(Fact f : allSourceFacts)
		{
			if(hasNonemptyIntersection(F,f))
			{
				refinedSourceFacts.add(f);
			}
		}
		Instance newSource = new Instance(refinedSourceFacts);
		refined.setSource(newSource);
		return refined;
	}
	
	// a helper function for blocking 
	public boolean hasNonemptyIntersection(Fact F1, Fact F2)
	{
		return Fact.hasNonemptyIntersection(F1, F2);
	}
	
	public HashSet<GavExample> computeOneCriticalConstraints(GavExample rawCounterExample, 
			GAVmapping goalMapping, OptimizedPostgresConnector postgresConnector )
	{
//		System.out.println("raw counter example = " + rawCounterExample);

		GavExample refinedCounterExample = blocking(rawCounterExample);
//		HashSet<GavExample> criticalSet = new HashSet<GavExample>();	
//		Fact F = refinedCounterExample.getTargetFact();
//
//		
//		HashSet<ArrayList<Fact>> consideredLHS = new HashSet<ArrayList<Fact>>();
//		
//		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
//		postgresConnector.initialization();
//		int iteration = 0;
//		boolean converged = true;
		
//		System.out.println("refined counter example = " + refinedCounterExample);
//		
		return computeJustOneCriticalSoundConstraint(refinedCounterExample, goalMapping, postgresConnector);
	}
	
//	public HashSet<GavExample> computeCriticalConstraints(GavExample rawCounterExample, GAVmapping goalMapping)
//	{
//		GavExample refinedCounterExample = blocking(rawCounterExample);
//		HashSet<GavExample> criticalSet = new HashSet<GavExample>();
//		ArrayList<GavExample> candidateQueue = new ArrayList<GavExample>();
//		Fact F = refinedCounterExample.getTargetFact();
//		candidateQueue.add(refinedCounterExample);
//		
////		System.out.println("computing critically sound constraint from = " + refinedCounterExample);
//		HashSet<ArrayList<Fact>> consideredLHS = new HashSet<ArrayList<Fact>>();
//		
//		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
//		postgresConnector.initialization();
//		int iteration = 0;
//		boolean converged = true;
//		while(!candidateQueue.isEmpty())
//		{
////			if(iteration%50==0)
////			{
////			System.out.print(iteration +"->");
////			}
//			if(iteration>=maxIteration)
//			{
//				converged = false;
//				break;
//			}
//			if(iteration%20==0)
//				System.out.println();
////			System.out.println();
//			GavExample current = candidateQueue.get(0);
////			System.out.println("computing critical constraints from =" + current);
//			candidateQueue.remove(0);			
//			
//			if(consideredLHS.contains(current.getSource().getFacts()))
//			{
//				continue;
//			}
//			else
//			{
////				System.out.println("adding = " + current.getSource().getFacts() );
//				consideredLHS.add(current.getSource().getFacts());
//			}
////			if(considered.contains(current))
////				continue;
////			considered.add(current);
//			
//			HashSet<GavExample> localCriticalSet = new HashSet<GavExample>();
//			for(int i = 0; i< current.getSource().getFacts().size(); i++)
//			{
//				ArrayList<Fact> tmp = new ArrayList<Fact>();
//				tmp.addAll(current.getSource().getFacts());
//				tmp.remove(i);
//				
//				if(consideredLHS.contains(tmp))
//					continue;
//				
////				System.out.println("creating a new example with facts="+ tmp);
//				Example newC = new Example();
//				Instance newSource = new Instance(tmp);
//				Instance newTarget = new Instance(new ArrayList<Fact>());
//				newC.setSource(newSource);
//				newC.setTarget(newTarget);
//				Set<String> relations = newC.getSource().getFactMap().keySet();
////				System.out.println("newly created = " + relations+" " +newC.getDatasourceTag());
//				
//				// commented out on 01/23/16 to test the new optimized connector
////				PostgresConnector.populateNewTableDDL(newC);
//				postgresConnector.populateNewTableDDL(newC);
//				
//				HashSet<Fact> chased = GAVmapping.chase(goalMapping, newC);
////				System.out.println("CHASED==="+chased);
//				if(chased.contains(F))
//				{
////					System.out.println(current+" can produce F="+ F);
//					GavExample newCritic = new GavExample();
//					newCritic.setSource(new Instance(tmp));
//					newCritic.setTargetFact(F);
//					localCriticalSet.add(newCritic);
//				}
////				consideredLHS.add(tmp);
//			}
//			if(localCriticalSet.isEmpty())
//			{
//				criticalSet.add(current);
//			}
//			else
//			{
//				candidateQueue.addAll(localCriticalSet);
//			}
//			iteration++;
//		}
//		System.out.println("# iterations performed = " + iteration);
//		postgresConnector.closeUp();
//		
//		if(converged)
//		{
//			return criticalSet;
//		}
//		else
//		{
//			System.out.println("Exceeding the limit of max iterations");
//			return computeJustOneCriticalSoundConstraint(refinedCounterExample, goalMapping, );
//		}
//	}
	
//	public OptimizedPostgresConnector postgresConnector = null;
	
	public HashSet<GavExample> computeJustOneCriticalSoundConstraint(GavExample counterExample, 
			GAVmapping goalMapping, OptimizedPostgresConnector postgresConnector)
	{
		HashSet<GavExample> criticalSet = new HashSet<GavExample>();
		Fact F = counterExample.getTargetFact();
		GavExample previous = new GavExample();
		previous.setSource(counterExample.getSource());
		previous.setTargetFact(F);
				
//		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
//		postgresConnector.initialization();
				
		GavExample candidate = new GavExample();
		candidate.setSource(previous.getSource());
		candidate.setTargetFact(F);
		
//		System.out.println("computing critical from  = " + candidate);
		
		for(int i = 0; i<candidate.getSource().getFacts().size(); i++)
		{
			ArrayList<Fact> tmp = new ArrayList<Fact>();
			tmp.addAll(candidate.getSource().getFacts());
			tmp.remove(i);
			
			Example newC = new Example();
			Instance newSource = new Instance(tmp);
			Instance newTarget = new Instance(new ArrayList<Fact>());
			newC.setSource(newSource);
			newC.setTarget(newTarget);
			
			postgresConnector.populateNewTableDDL(newC);
			HashSet<Fact> chased = GAVmapping.chase(goalMapping, newC);
			if(chased.contains(F))
			{
				// the current fact can be removed
//				System.out.println(current+" can produce F="+ F);
				GavExample newCritic = new GavExample();
				newCritic.setSource(new Instance(tmp));
				newCritic.setTargetFact(F);
				candidate.getSource().getFacts().remove(i);
				i--;
			}
			else
			{
				// we cannot remove current fact from the candidate LHS
				// in this case, do nothing, and move to next fact
			}
		}
//		System.out.println("after computing critical from  = " + candidate);

		criticalSet.add(candidate);
//		postgresConnector.closeUp();
		return criticalSet;
		
//		
//		while(true)
//		{
//			GavExample current = new GavExample();
//			current.setSource(previous.getSource());
//			current.setTargetFact(F);
//			boolean hasFound = false;
//			
//			System.out.println("computing ONE critical constraint from =" + current);
//
//			for(int i = 0; i< current.getSource().getFacts().size(); i++)
//			{
//				ArrayList<Fact> tmp = new ArrayList<Fact>();
//				tmp.addAll(current.getSource().getFacts());
//				tmp.remove(i);
//				
//				
////				System.out.println("creating a new example with facts="+ tmp);
//				Example newC = new Example();
//				Instance newSource = new Instance(tmp);
//				Instance newTarget = new Instance(new ArrayList<Fact>());
//				newC.setSource(newSource);
//				newC.setTarget(newTarget);
////				Set<String> relations = newC.getSource().getFactMap().keySet();
////				System.out.println("newly created = " + relations+" " +newC.getDatasourceTag());
//				
//				postgresConnector.populateNewTableDDL(newC);
//				
//				HashSet<Fact> chased = GAVmapping.chase(goalMapping, newC);
//				if(chased.contains(F))
//				{
////					System.out.println(current+" can produce F="+ F);
//					GavExample newCritic = new GavExample();
//					newCritic.setSource(new Instance(tmp));
//					newCritic.setTargetFact(F);
//					previous = newCritic;
//					current = previous;
//				}
//				else
//				{
//					hasFound = true;
//					break;					
//				}
//			}
//			if(hasFound)
//				break;
//		}
//		criticalSet.add(previous);
//		postgresConnector.closeUp();
//		return criticalSet;
	}
	
	
	
	public GavExample isC_Equivalent()
	{
		for(int k = 0 ; k < C.size(); k++)
		{
			Example e = C.get(k);
//			System.out.println("considering " + e);
			GavExample counterexample = null;
			if(e.getSource().getFacts().isEmpty())
				continue;
			
			if((counterexample=agreeOn(candidate, e))!=null)
			{
				return counterexample;
			}
			else
			{
//				System.out.println("adding =" + e);
//				System.out.println("CHECKED="+ checked);
				checked.add(e);
			}
		}
		return null;
	}
	/**
	 * 
	 * Unfinished
	 * @param candidate
	 * @param goal
	 * @param e
	 * @return
	 */
	public GavExample agreeOn(GAVmapping candidate, Example e)
	{
		// under construction
//		System.out.println("candidate="+candidate.gavConstraints);
//		System.out.println("CANDIDATE MAPPING = " + candidate);
		HashSet<Fact> chasedTarget = GAVmapping.chase(candidate, e); 
		//		System.out.println("chasedTarget="+chasedTarget);
		if(!sameTargetInstance(chasedTarget, e))
		{
//			System.out.println("the candidate mapping and goal mapping do not agree on = \n" + e);
			// construct a counter example (I,J')
			// where I is identical to e.getSource(), and J' contains a single fact F that is contained in e.targetInstance()
			// but not in chasedTarge
			GavExample counterexample = new GavExample();
			counterexample.setSource(e.getSource());
			// compute e.targetInstance()\chasedTarget
			ArrayList<Fact> setDifference = setMinus(e.getTarget(), chasedTarget);
//			System.out.println("Choose F from "+setDifference);
			if(setDifference.size()>0)
			{
				// the universal solutin in consideration has some fact that 
				// is not contained in the chase result.
				Fact F = setDifference.get(0);
				counterexample.setTargetFact(F);
				return counterexample;
			}
			else
			{
				return null;
			}
		}
		else
		{
//			System.out.println("SAME AS THE UNIVERSAL EXAMPLE IN CONSIDERATION");
			return null;
		}
	}
	
	public ArrayList<Fact> setMinus(Instance targetInstance, HashSet<Fact> chasedTarget)
	{
		ArrayList<Fact> currentTarget = new ArrayList<Fact>();
		currentTarget.addAll(targetInstance.getFacts());
		currentTarget.removeAll(chasedTarget);
		return currentTarget;
		
	}
	
	public boolean sameTargetInstance(HashSet<Fact> chasedResult, Example universal)
	{
		ArrayList<Fact> standard = universal.getTarget().getFacts();
		if(standard.size()!= chasedResult.size())
			return false;
		else
		{
			chasedResult.removeAll(standard);
			return chasedResult.size()==0;
		}
	}

	public ArrayList<Example> getC() {
		return C;
	}

	public void setC(ArrayList<Example> c) {
		C = c;
	}

	public ArrayList<Example> getChecked() {
		return checked;
	}

	public void setChecked(ArrayList<Example> checked) {
		this.checked = checked;
	}

	public GAVmapping getCandidate() {
		return candidate;
	}

	public void setCandidate(GAVmapping candidate) {
		this.candidate = candidate;
	}

	public GAVmapping getGoalMapping() {
		return goalMapping;
	}

	
}
