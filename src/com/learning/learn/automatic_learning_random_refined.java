package com.learning.learn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.learning.evaluation.LearningResult;
import com.learning.evaluation.mappingEvaluator;
import com.learning.example.Example;
import com.learning.example.Fact;
import com.learning.example.GavExample;
import com.learning.example.Instance;
import com.learning.ibench.adapters.iBenchMappingParser;
import com.learning.ibench.adapters.iBenchMappingScenario;
import com.learning.mapping.GAVconstraint;
import com.learning.mapping.GAVmapping;
import com.learning.mapping.Param;

public class automatic_learning_random_refined {
	final static int evaluationID = 1;
	final static int numOfComprehensive = 1;
	final static String pathToLearnedMappings = "/home/kun/code/bitbucketRepo/"
			+ "duoduo5813-gav_learning-dddf27358125/GAVLearning/learnedMappings/";

	final static int numRandomRunsOfTrainingEvaluation = 3;
	
	final static long randomSeedUpperbound 			= 2234567890l;


	static int[] numOfExamples = { 10, 20, 30, 40, 50, 60, 70, 80 };
	static double[] selectivities = { 0.1, 0.2, 0.3, 0.4 };
	static double[] TRAINING_PERCENTAGES = { 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5 };

	//
	// static int[] numOfExamples = {10, 20, 30, 40, 50, 60, 70, 80, 90};
	// static double[] selectivities = {0.01, 0.002, 0.003, 0.004, 0.005};
	// static double[] TRAINING_PERCENTAGES = {0.2, 0.2, 0.3, 0.4, 0.5, 0.5,
	// 0.5, 0.5, 0.5};

	public static void main(String[] args) throws IOException {
		String learningResultsPath = "/home/kun/code/bitbucketRepo/"
				+ "duoduo5813-gav_learning-dddf27358125/GAVLearning/learningResults.log";
		FileWriter resultsWriter = new FileWriter(learningResultsPath);

		final String mappingComplexity = "SIMPLE_10Facts";
		// sample config filename: SIMPLE_40percent_EX_1
		// MAPPINGCOMPLEXITY_selectivitypercent_EXAMPLE_ID
		String configfileNameTemplate = "%s_%dpercent_EXSIZE%d_RUN%d_EX_%d";
		String experimentDescriptionTemplate = "%s_%dpercent_EXSIZE%d_RUN%d";
		
		cleanMappingScenarios();
		creatingMappingScenarioFolder();
		String pathToProperties = "/home/kun/code/bitbucketRepo/duoduo5813-gav_learning-dddf27358125/GAVLearning/learningConfig";

		for (double selectivity : selectivities) {
			int index = 0;
			for (; index < numOfExamples.length; index++) 
			{
				int currNumOfExamples = numOfExamples[index];
				double currTrainingPercentage = TRAINING_PERCENTAGES[index];
				int numOfTrainExamples = (int) (currNumOfExamples * currTrainingPercentage);

				cleanRandomInstances();
				iBenchMappingParser iBenchParser = new iBenchMappingParser(pathToProperties);
				PostgresConnector.resetGavLearningDB();
				OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
				postgresConnector.initialization();
				GavLearner learner = new GavLearner(pathToProperties);

				// // Step 1. Fix the mapping, and then create a configuration
				// file for a new comprehensive source instance
				// scenario.buildConfigFileForiBench_random(iBenchParser.getIBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH(),
				// iBenchParser.getIBENCH_ROOT(), evaluationID);
				iBenchMappingScenario scenario = iBenchParser.parse();
				scenario.setPropertiesObject(pathToProperties);

				// Step 1. Fix the mapping, and then create a configuration file
				// for a new comprehensive source instance

				resultsWriter.write("*******************************************************************\n");
				resultsWriter.write(String.format("** Mapping Scenario:%s\t NumOfExample:%d\t Selectivity=%.2f **\n",
						mappingComplexity, currNumOfExamples, selectivity));
				resultsWriter.write("*******************************************************************\n");
				resultsWriter.flush();

				System.out.print("Constructing source instance.\n");
				long RandomSeedForMappingGeneration = (long)(Math.random()*(randomSeedUpperbound+1));
					
				ArrayList<LearningResult> learningResults = new ArrayList<LearningResult>();
				for(int k = 0 ; k < numRandomRunsOfTrainingEvaluation; k++)
				{
					boolean isTrivial = false;
					ArrayList<Example> randomUniversal = new ArrayList<Example>();
					for (int exampleID = 1; exampleID <= currNumOfExamples; exampleID++) 
					{
						String newConfigFileName = String.format(configfileNameTemplate, mappingComplexity,
								(int) (selectivity * 100), currNumOfExamples, 
								k+1,
								exampleID);
						String newConfigFileFullPath = Keywords.RANDOM_SCENARIOS_FOLDER + "/" + newConfigFileName;
						// System.out.println("EXAMPLE_ID="+ exampleID);
						scenario.buildConfigFileForiBench_random2(iBenchParser.getIBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED(),
								iBenchParser.getIBENCH_ROOT(), newConfigFileName, exampleID,
								RandomSeedForMappingGeneration);

//						System.out.println("new Config = " + newConfigFileFullPath);
						String workFolder = iBenchParser.getIBENCH_ROOT();
						runIBENCH(workFolder, newConfigFileFullPath, exampleID);
						iBenchParser.TriggerRandom(true);
						String param = scenario.getParameters();
						String outFolderName = "out_"+ newConfigFileName;
						scenario = iBenchParser.parse_REFINED(outFolderName);
						scenario.setPropertiesObject(pathToProperties);
						scenario.setParameters(param);
						String scenarioName = newConfigFileName;
						scenario.outputScenario_REFINED(scenarioName);
						Example randomExample = iBenchMappingScenario.createExample(scenario);
						if(randomExample.getSource().getFactMap().size()<1)
						{
							System.out.println("Trivial examples");
							exampleID--;
//							scenario.buildConfigFileForiBench_random2(iBenchParser.getIBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH_REFINED(),
//									iBenchParser.getIBENCH_ROOT(), newConfigFileName, exampleID,
//									RandomSeedForMappingGeneration);		
							RandomSeedForMappingGeneration = 
									(long)(Math.random()*(randomSeedUpperbound+1));
							isTrivial = true;
							break;
						}
						Instance newRandomSource = 
								iBenchMappingScenario.createRandomSourcesWithFixedSelectivity_REFINED(randomExample,
								selectivity);
						
//						System.out.print(" -- chasing --");
						Example oneRandomUniversal = 
								scenario.createRandomUniversalExamples_REFINED(scenario.getGoalMapping(), 
										newRandomSource, postgresConnector);
						randomUniversal.add(oneRandomUniversal);
//						System.out.println("(Finished).");
						
					}
					if(isTrivial)
					{
						k--;
						continue;
					}
					int count =0;
					for(Example e : randomUniversal)
					{
						System.out.print(e.getSource().getFacts().size()+" ");
						count+= e.getSource().getFacts().size();
					}
					resultsWriter.write("Average Size of Source instance = " + count/((double)randomUniversal.size())+"\n");
					
					System.out.print(String.format("\n3. preparing for the %d-th Training-Evaluation ", (k+1)));
					//resultsWriter.write(String.format("\n3. preparing for the %d-th Training-Evaluation ", (k+1)));
					ArrayList<Example> randEvaluation  = new ArrayList<Example>(randomUniversal);
					
					// create a random training data
					ArrayList<Example> randTraining = createRandomTraining(randomUniversal, numOfTrainExamples);
					// create a random evaluation data
					randEvaluation.removeAll(randTraining);
					
					System.out.println("TRAINING_SIZE="+ randTraining.size());
					System.out.println("Evaluation_SIZE="+ randEvaluation.size());

					String currExperimentDescription = String.format(experimentDescriptionTemplate, mappingComplexity,
							(int) (selectivity * 100), currNumOfExamples, 
							k+1);
					
					String trainingDataFileName = String.format("TRAIN_%s", currExperimentDescription);
					String testDataFileName 	= String.format("TEST_%s", currExperimentDescription);

					scenario.materializeRandomUniversalsToTRAIING_DATA(scenario, randTraining, trainingDataFileName);
					scenario.materializeRandTestExamplesToTest_REFINED(scenario, randEvaluation, testDataFileName);
					System.out.println("(Finished).");
					//resultsWriter.write("(Finished).\n");
					
					GAVmapping goal = scenario.getGoalMapping();
					double sizeOfGoalMapping = goal.gavExamples.size();
					long startOfChecking = System.currentTimeMillis();
					System.out.println("checking training comprehensiveness");
					/**
					 * Following checking function needs to be refined
					 */
					HashMap<GAVconstraint, Integer> triggeredConstraints = 
							getCompleteTriggerInformation(goal, randTraining);
//					long endOfChecking = System.currentTimeMillis();
//					System.out.println("TRIGGERED = " + triggered_in_training.size());
//					System.out.println("Checking finished in = "+ (endOfChecking-startOfChecking)/1000.0);
//					resultsWriter.write("Checking finished in = "+ (endOfChecking-startOfChecking)/1000.0 + "\n");
					
//					learner = new GavLearner(pathToProperties);
					learner.setOptimizedPOstgresConnector(postgresConnector);

					learner.setPathToUniversalExamples(scenario.getPathToTrainingData());
					learner.setGoalMapping(scenario.getGoalMapping());
					learner.initialization();
					learner.setMaxIteration(learner.getMAX_LEARNING_ITERATION());
					long start = System.currentTimeMillis();
					learner.learn();
					long end = System.currentTimeMillis();
					int totalLearningIterations = learner.getTotalNumberLearningIteration();
					System.out.println(String.format("The %d-th learning has finished", (k+1)));
					//resultsWriter.write("\n");
					
					GAVmapping learnedMapping = learner.getCandidate();
					HashSet<GavExample> mappingAsExamples = learnedMapping.gavExamples;
					String currentLearnedMappingName = 
							String.format("Mapping_%s", currExperimentDescription);
					storeCurrentGavMappingAsExamples(mappingAsExamples, 
							scenario, pathToLearnedMappings, currentLearnedMappingName);
					
					String pathToLearned =  pathToLearnedMappings + currentLearnedMappingName;
					String pathToTestSet =  scenario.getPathToTestData();
					
					System.gc();
					mappingEvaluator evaluator = new mappingEvaluator(pathToTestSet, pathToLearned);
					
					LearningResult lr = evaluator.computePrecision_Recall_Fscore();
					lr.setRuntime((end-start));
//					double degreeOfComprehensive_Training   = (double)triggeredConstraints.size() / sizeOfGoalMapping;
//					lr.setDegreeOfComp_Training(degreeOfComprehensive_Training);
					lr.setTriggeredStatistics(triggeredConstraints);
					lr.setTotalNumberOfIterations(totalLearningIterations);
					lr.computeDetailedComprehensiveness();
					System.out.println(lr);
					learningResults.add(lr);
					resultsWriter.flush();
				}
				System.out.println("\n\nLEARNING DONE");
				//resultsWriter.write("\n\nLEARNING DONE\n");
				System.out.println(scenario.getParameters());
				for(LearningResult lr : learningResults)
				{
					System.out.println(lr);
					resultsWriter.write(lr+"\n");
				}
				double avgPrecision = LearningResult.avgPrecision(learningResults);
				double avgRecall    = LearningResult.avgRecall(learningResults);
				
				System.out.print(String.format("avg_Prec=%.3f", avgPrecision));
				System.out.print(String.format("\tavg_Recall=%.4f", avgRecall));
				System.out.print(String.format("\tF-score=%.4f", LearningResult.computeFscore(avgPrecision, avgRecall)));
				System.out.print(String.format("\tavg_DOC=%.3f", 
						LearningResult.avgDegreeOfComprehensiveness(learningResults)));
				System.out.print(String.format("\tDeviation_Prec=%.3f", LearningResult.computePrecisionDeviation(learningResults, avgPrecision)));
				System.out.print(String.format("\tDeviation_Recall=%.3f", LearningResult.computeRecallDeviation(learningResults, avgRecall)));
				System.out.println(String.format("\tavg_Runtime=%s", LearningResult.avgRuntime(learningResults)));
				
				resultsWriter.write(String.format("avg_Prec=%.3f", avgPrecision));
				resultsWriter.write(String.format("\tavg_Recall=%.4f", avgRecall));
				resultsWriter.write(String.format("\tF-score=%.4f", LearningResult.computeFscore(avgPrecision, avgRecall)));
				resultsWriter.write(String.format("\tavg_DOC=%.3f", 
						LearningResult.avgDegreeOfComprehensiveness(learningResults)));
				resultsWriter.write(String.format("\tavg_Triggers=%.3f", LearningResult.avgNumTriggers(learningResults)));
				resultsWriter.write(String.format("\tDeviation_Prec=%.5f", LearningResult.computePrecisionDeviation(learningResults, avgPrecision)));
				resultsWriter.write(String.format("\tDeviation_Recall=%.5f", LearningResult.computeRecallDeviation(learningResults, avgRecall)));
				resultsWriter.write(String.format("\tavg_Iterations=%.2f", LearningResult.avgLearningIteration(learningResults)));
				resultsWriter.write(String.format("\tavg_Runtime=%s", LearningResult.avgRuntime(learningResults)));
//				for(LearningResult lr : learningResults)
//				{
//					resultsWriter.write(String.format("\nTriggeredInfo=%s\n", lr.computeDetailedComprehensiveness()));
//				}

				resultsWriter.write("\n\n\n");
				resultsWriter.flush();
				postgresConnector.closeUp();
			}
		}
		resultsWriter.close();

	}

	public static HashMap<GavExample, GAVconstraint> triggeredMap = new HashMap<GavExample, GAVconstraint>();

	public static HashMap<GAVconstraint, Integer> 
		getCompleteTriggerInformation(GAVmapping goal, ArrayList<Example> examples)
	{
		HashMap<GAVconstraint, Integer> triggeredConstraints = new HashMap<GAVconstraint, Integer>();
		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		for (GavExample c : goal.gavExamples) {
			GAVconstraint currConstraint = GAVconstraint.createConstraint(c);
			triggeredConstraints.put(currConstraint, 0);
			GAVmapping dummyMapping = new GAVmapping();
			dummyMapping.addConstraint(c);
			// System.out.println("current Constraint = " + c);

//			if (triggeredMap.containsKey(c)) {
//				// System.out.println("found a tgd that has been previously triggered");
//				triggeredConstraint.add(triggeredMap.get(c));
//				continue;
//			}
			for (Example example : examples) {
				ArrayList<Fact> tmp = new ArrayList<Fact>();
				tmp.addAll(example.getSource().getFacts());
				Example newC = new Example();
				Instance newSource = new Instance(tmp);
				Instance newTarget = new Instance(new ArrayList<Fact>());
				newC.setSource(newSource);
				newC.setTarget(newTarget);

				postgresConnector.populateNewTableDDL(newC);
				// System.out.println("NewC= " + newC);
				HashSet<Fact> chased = GAVmapping.chase(dummyMapping, newC);
				if (chased.size() > 0) {
					if(!triggeredConstraints.containsKey(currConstraint))
					{
						triggeredConstraints.put(currConstraint, 1);
					}
					else
					{
						int previousTriggeredTimes = triggeredConstraints.get(currConstraint);
						triggeredConstraints.put(currConstraint, previousTriggeredTimes+1);
					}
//					triggered.add(c);
//					triggeredConstraint.add(GAVconstraint.createConstraint(c));
//					triggeredMap.put(c, GAVconstraint.createConstraint(c));
//					break;
				}
			}
		}

		postgresConnector.closeUp();
		return triggeredConstraints;
	}
	public static HashSet<GAVconstraint> getTriggeredConstraintsOptimizied(GAVmapping goal, 
			ArrayList<Example> examples) {
		ArrayList<GavExample> triggered = new ArrayList<GavExample>();
		HashSet<GAVconstraint> triggeredConstraint = new HashSet<GAVconstraint>();

		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();

		for (GavExample c : goal.gavExamples) {
			GAVmapping dummyMapping = new GAVmapping();
			dummyMapping.addConstraint(c);
			boolean isTriggered = false;
			// System.out.println("current Constraint = " + c);

			if (triggeredMap.containsKey(c)) {
				// System.out.println("found a tgd that has been previously triggered");
				triggeredConstraint.add(triggeredMap.get(c));
				continue;
			}
			for (Example example : examples) {
				ArrayList<Fact> tmp = new ArrayList<Fact>();
				tmp.addAll(example.getSource().getFacts());
				Example newC = new Example();
				Instance newSource = new Instance(tmp);
				Instance newTarget = new Instance(new ArrayList<Fact>());
				newC.setSource(newSource);
				newC.setTarget(newTarget);

				postgresConnector.populateNewTableDDL(newC);
				// System.out.println("NewC= " + newC);
				HashSet<Fact> chased = GAVmapping.chase(dummyMapping, newC);
				if (chased.size() > 0) {
					// System.out.println("This constraint IS triggered = " +
					// c);
					isTriggered = true;
					triggered.add(c);
					triggeredConstraint.add(GAVconstraint.createConstraint(c));
					triggeredMap.put(c, GAVconstraint.createConstraint(c));
					break;
				}
			}
			if (!isTriggered) {
				// System.out.println("This constraint is not triggered = " +
				// c);
			}
		}

		postgresConnector.closeUp();

		return triggeredConstraint;
	}

	public static ArrayList<GAVconstraint> getTriggeredConstraints(GAVmapping goal, ArrayList<Example> examples) {
		ArrayList<GavExample> triggered = new ArrayList<GavExample>();
		ArrayList<GAVconstraint> triggeredConstraint = new ArrayList<GAVconstraint>();

		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();

		for (GavExample c : goal.gavExamples) {
			GAVmapping dummyMapping = new GAVmapping();
			dummyMapping.addConstraint(c);
			boolean isTriggered = false;
			// System.out.println("current Constraint = " + c);

			for (Example example : examples) {
				ArrayList<Fact> tmp = new ArrayList<Fact>();
				tmp.addAll(example.getSource().getFacts());
				Example newC = new Example();
				Instance newSource = new Instance(tmp);
				Instance newTarget = new Instance(new ArrayList<Fact>());
				newC.setSource(newSource);
				newC.setTarget(newTarget);

				postgresConnector.populateNewTableDDL(newC);
				// System.out.println("NewC= " + newC);
				HashSet<Fact> chased = GAVmapping.chase(dummyMapping, newC);
				if (chased.size() > 0) {
					// System.out.println("This constraint IS triggered = " +
					// c);
					isTriggered = true;
					triggered.add(c);
					triggeredConstraint.add(GAVconstraint.createConstraint(c));
					break;
				}
			}
			if (!isTriggered) {
				// System.out.println("This constraint is not triggered = " +
				// c);
			}
		}

		postgresConnector.closeUp();

		return triggeredConstraint;
	}

	public static boolean checkComprehensivenessForEvaluation(GAVmapping goal, ArrayList<Example> train) {
		boolean isComprehensive = true;
		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();

		for (GAVconstraint c : goal.gavConstraints) {
			GAVmapping dummyMapping = new GAVmapping();
			dummyMapping.addConstraint(c);
			boolean isTriggered = false;
			for (Example example : train) {
				ArrayList<Fact> tmp = new ArrayList<Fact>();
				tmp.addAll(example.getSource().getFacts());
				Example newC = new Example();
				Instance newSource = new Instance(tmp);
				Instance newTarget = new Instance(new ArrayList<Fact>());
				newC.setSource(newSource);
				newC.setTarget(newTarget);

				postgresConnector.populateNewTableDDL(newC);
				HashSet<Fact> chased = GAVmapping.chase(dummyMapping, newC);
				if (chased.size() > 0) {
					isTriggered = true;
					break;
				}
			}
			if (!isTriggered) {
				System.out.println("This constraint is not triggered in the evaluation data = " + c);
				postgresConnector.closeUp();
				return false;
			}
		}

		postgresConnector.closeUp();

		return isComprehensive;
	}

	public static ArrayList<Example> createRandomTraining(ArrayList<Example> pool, int numOfTraining) {
		ArrayList<Example> randResult = new ArrayList<Example>();
		ArrayList<Example> copy = new ArrayList<Example>(pool);

		while (numOfTraining > 0) {
			int remainingSize = copy.size();
			int index = (int) (Math.random() * remainingSize);
			randResult.add(copy.get(index));
			copy.remove(index);
			numOfTraining--;
		}
		return randResult;
	}

	public static void storeCurrentGavMappingAsExamples(
			HashSet<GavExample> learnedMapping, 
			iBenchMappingScenario scenario,
			String path,
			String learnedMappingName){
		StringBuffer sb = new StringBuffer();

		for (GavExample example : learnedMapping) {
			// System.out.println("Storing = "+ example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(scenario.formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(example.getTargetFact());
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
		// System.out.println(sb.toString());
		try {
			FileWriter writer = new FileWriter(path + learnedMappingName);
			writer.append(sb.toString());
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static void storeGavMappingAsExamples(HashSet<GavExample> learnedMapping, iBenchMappingScenario scenario,
			String path) {
		StringBuffer sb = new StringBuffer();

		for (GavExample example : learnedMapping) {
			// System.out.println("Storing = "+ example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(scenario.formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(example.getTargetFact());
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
		// System.out.println(sb.toString());
		try {
			FileWriter writer = new FileWriter(path + "learnedMapping");
			writer.append(sb.toString());
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ArrayList<Example> createTrainingSet(ArrayList<Example> allE, int size) {
		ArrayList<Example> result = new ArrayList<Example>();
		int count = 0;
		while (count < size) {
			result.add(allE.get(count));
			count++;
		}
		return result;
	}

	public static ArrayList<Example> createTestset(ArrayList<Example> allE, int size) {
		ArrayList<Example> result = new ArrayList<Example>();
		for (int k = allE.size() - 1; size > 0; k--) {
			result.add(allE.get(k));
			size--;
		}
		return result;
	}

	public static void cleanMappingScenarios() {
		try {
			ProcessBuilder pb = new ProcessBuilder("rm",  "-r", "mappingScenarios");
			pb.directory(new File("/home/kun/code/bitbucketRepo"
					+ "/duoduo5813-gav_learning-dddf27358125/GAVLearning/src/"));
			Process p = pb.start();
			p.waitFor();
			StringBuffer sb = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

			System.out.println("RESETTING:"+ sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void creatingMappingScenarioFolder() {
		try {
			ProcessBuilder pb = new ProcessBuilder("mkdir", "mappingScenarios");
			pb.directory(new File("/home/kun/code/bitbucketRepo"
					+ "/duoduo5813-gav_learning-dddf27358125/GAVLearning/src/"));
			Process p = pb.start();
			p.waitFor();
			StringBuffer sb = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

			System.out.println("RESETTING:"+ sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void cleanRandomInstances() {
		try {
			ProcessBuilder pb = new ProcessBuilder("rm", "-r", "out0/");
			pb.directory(new File("/kun/coding/ibench/build/mac/random"));
			Process p = pb.start();
			p.waitFor();
			StringBuffer sb = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

			System.out.println(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void cleanComprehensiveInstances() {
		try {
			ProcessBuilder pb = new ProcessBuilder("rm", "-r", "out0/");
			pb.directory(new File("/kun/coding/ibench/build/mac/comprehensive"));
			Process p = pb.start();
			p.waitFor();
			StringBuffer sb = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

			System.out.println(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void runIBENCH(String folder, String configFile, int iteration) {
		try {
			// sh iBenchMac.sh -c gavtest0
			// System.out.println(folder);
			ProcessBuilder pb = new ProcessBuilder("sh", Keywords.IBENCH_SHELL_SCRIPT_NAME, "-c", configFile);
			pb.directory(new File(folder));
//			System.out.println("folder* = " + folder);

			Process p = pb.start();
			p.waitFor();
			StringBuffer sb = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
