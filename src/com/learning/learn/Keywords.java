package com.learning.learn;

public class Keywords {

	public static String XML_SCHEMA_ROOT  		= "/MappingScenario/Schemas";
	public static String XML_SOURCE_SCHEMA_NODE = "SourceSchema";
	public static String XML_TARGET_SCHEMA_NODE = "TargetSchema";
	public static String XML_RELATION_NODE 		= "Relation";
	public static String XML_NAME_ATTRIBUTE 	= "name";
	public static String XML_RELATION_ATTR_NODE	= "Attr";
	public static String XML_COLUMN_NAME_NODE   = "Name";
	
	public static String XML_PATH_SOURCESCHEMA 	= XML_SCHEMA_ROOT+"/"+XML_SOURCE_SCHEMA_NODE;
	public static String XML_PATH_TARGETSCHEMA 	= XML_SCHEMA_ROOT+"/"+XML_TARGET_SCHEMA_NODE;
	
	
	// keywords for mapping part
	public static String XML_MAPPINGS_ROOT 			= "/MappingScenario/Mappings";
	public static String XML_MAPPING_NODE			= "Mapping";
	public static String XML_MAPPING_ID_ATTR   		= "id";
	public static String XML_MAPPING_FOREACH_NODE	= "Foreach";
	public static String XML_MAPPING_EXISTS_NODE	= "Exists";
	public static String XML_MAPPING_ATOM_NODE		= "Atom";
	public static String XML_ATOM_TABLEREF_ATTR     = "tableref";
	public static String XML_MAPPING_ATOM_VAR       = "Var";
	
	public static String XML_PATH_TO_MAPPINGS 		= XML_MAPPINGS_ROOT+"/"+XML_MAPPING_NODE;

	// keywords for output files of iBench
	public static String OUTFILE_SCHEMAS_PREFIX  	= "Schemas";
	public static String XML_EXTENSION				= ".xml";
	public static String CSV_EXTENSION				= ".csv";
	public static String CSV_DELIMITER				= "|";
	
	public static String IBENCH_ROOT					= "IBENCH_ROOT";
	public static String IBENCH_MAPPING_OUT_FOLDER		= "IBENCH_MAPPING_OUT_FOLDER";
	public static String IBENCH_MAPPING_CONFIG_FILENAME	= "IBENCH_MAPPING_CONFIG_FILENAME";
	public static String WORK_ROOT						= "WORK_ROOT";
	public static String UNIVERSAL_EXAMPLE_C_PATH		= "UNIVERSAL_EXAMPLE_C_PATH";
	public static String MAPPING_SCENARIO_FILENAME		= "MAPPING_SCENARIO_FILENAME";
	
	
	// learning paramters
	public static String NUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES	= "NUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES";
	public static String TESTSET_PERCENTAGE						= "TESTSET_PERCENTAGE";
	public static String MAX_LEARNING_ITERATION					= "MAX_LEARNING_ITERATION";
	public static String RANDOM_SELECTIVITY_LOWERBOUND			= "RANDOM_SELECTIVITY_LOWERBOUND";

	
	public static String CONFIGFILE_TEMPLATE_FILENAME 			= "configTemplate";
	public static String CONFIGFILE_TEMPLATE_FILENAME_Random	= "configTemplate_random";
	public static String CONFIGFILE_TEMPLATE_FILENAME_Random_REFINED	= "configTemplate_random2";

	public static String COMPREHENSIVE_SCENARIOS_FOLDER			= "comprehensive";
	public static String RANDOM_SCENARIOS_FOLDER				= "random";
	public static String CONFIG_FILENAME 			  			= "gavtest";
	public static String IBENCH_SHELL_SCRIPT_NAME 	  			= "iBenchMac.sh";
	public static String UNIVERSAL_EXAMPLE_TEST_COMPREHENSIVE 	= "COMPTEST";
	public static String UNIVERSAL_EXAMPLE_TEST_RANDOM		 	= "RANDTEST";
	
	public static String MAPPING_SCENARIO_FOLDER		 	= "mappingScenarios";
	public static String TRAINING_DATA_FOLDER				= "TRAINING";
	public static String TEST_DATA_FOLDER					= "TEST";

	
}
