package com.learning.learn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.learning.evaluation.LearningResult;
import com.learning.evaluation.mappingEvaluator;
import com.learning.example.Example;
import com.learning.example.Fact;
import com.learning.example.GavExample;
import com.learning.example.Instance;
import com.learning.ibench.adapters.iBenchMappingParser;
import com.learning.ibench.adapters.iBenchMappingScenario;
import com.learning.mapping.GAVconstraint;
import com.learning.mapping.GAVmapping;
import com.learning.mapping.Param;

public class learnFromRandomIns_RandomTrainingEvaluation {
	final static int evaluationID = 1;
	final static int numOfComprehensive = 1;
	final static String pathToLearnedMappings = "/home/kun/code/bitbucketRepo/"
			+ "duoduo5813-gav_learning-dddf27358125/GAVLearning/learnedMappings/";
	
	final static int numRandomRunsOfTrainingEvaluation = 3;
	
	public static void main(String[] args)
	{
		cleanRandomInstances();
		String pathToProperties = "/home/kun/code/bitbucketRepo/duoduo5813-gav_learning-dddf27358125/GAVLearning/learningConfig";
		iBenchMappingParser iBenchParser = new iBenchMappingParser(pathToProperties);
		PostgresConnector.resetGavLearningDB();
		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		GavLearner learner = new GavLearner(pathToProperties);

		double selectivityLowerBound    = learner.getRANDOM_SELECTIVITY_LOWERBOUND();
		int    numOfExamples 			= learner.getNUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES();
		double testsetPerc 				= (double)(learner.getTESTSET_PERCENTAGE())/100f;
		int numOfTrainExamples    		= (int)(numOfExamples*testsetPerc);
		
		iBenchMappingScenario scenario = iBenchParser.parse();
		scenario.setPropertiesObject(pathToProperties);
		
		// Step 1. Fix the mapping, and then create a configuration file for a new comprehensive source instance
		scenario.buildConfigFileForiBench_random(iBenchParser.getIBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH(), 
							iBenchParser.getIBENCH_ROOT(), evaluationID);		

		
		// step 2. Run iBench a number of times to get different comprehensive source instances
	    System.out.print("Constructing source instance.\n");
		for(int exampleID = 1; exampleID <= 1; exampleID++)
		{
			System.out.print("Creating "+ exampleID+"-th\tSource Instance ");
			String newConfigFileName = Keywords.RANDOM_SCENARIOS_FOLDER+"/"+Keywords.CONFIG_FILENAME+evaluationID;
			System.out.println("new Config = " + newConfigFileName);
			String workFolder  		 = iBenchParser.getIBENCH_ROOT();
			runIBENCH(workFolder, newConfigFileName,  exampleID);

		    
//		    System.out.print("1.Parsing iBench Mapping Scenario ");
		    iBenchParser.TriggerRandom(true);
		    String param = scenario.getParameters();
		    scenario = iBenchParser.parse();
			scenario.setPropertiesObject(pathToProperties);
			scenario.setParameters(param);
			scenario.outputScenario();

//			scenario.outputScenario();
//			System.out.println("(Finished).");

			
			Example randomExample = iBenchMappingScenario.createExample(scenario);
			if(randomExample.getSource().getFactMap().size()<1)
			{
				exampleID--;
				scenario.buildConfigFileForiBench_random(iBenchParser.getIBENCH_CONFIGFILE_TEMPLATE_RANDOM_PATH(), 
						iBenchParser.getIBENCH_ROOT(), evaluationID);		
				continue;
			}
//			allRandSources.add(randomExample);
			System.out.println("(finished)");

			
			System.out.print(String.format("2.Creating %d random universal examples ", numOfExamples));
//			ArrayList<Instance> randomSources = 
//					iBenchMappingScenario.createRandomSources_RandomSelectivity_WithLowerBound(randomExample, numOfExamples, selectivityLowerBound);
			System.out.println("SELECTIVITY=" +selectivityLowerBound );
			ArrayList<Instance> randomSources = 
					iBenchMappingScenario.createRandomSourcesWithFixedSelectivity(randomExample, numOfExamples, selectivityLowerBound);
			

//			ArrayList<Example> randomUniversal = 
//					scenario.createRandomUniversalExamples(scenario.getGoalMapping(), 
//							randomSources, triggeredMap,  postgresConnector);
			System.out.print(" -- chasing --");
			ArrayList<Example> randomUniversal = 
					scenario.createRandomUniversalExamples(scenario.getGoalMapping(), randomSources, postgresConnector);
			
			
			System.out.println("(Finished).");
			//System.out.println("nbr of random examples = " + randomUniversal.size());
			System.out.println("sizes of source instances");
			for(Example e : randomUniversal)
			{
				System.out.print(e.getSource().getFacts().size()+" ");
			}
			System.out.println();
			
			System.out.println("Goal Mapping = " + scenario.getGoalMapping());
			
			ArrayList<LearningResult> learningResults = new ArrayList<LearningResult>();
			
			for(int k = 0 ; k < numRandomRunsOfTrainingEvaluation; k++)
			{
				System.out.print(String.format("\n3. preparing for the %d-th Training-Evaluation ", (k+1)));
				ArrayList<Example> randEvaluation  = new ArrayList<Example>(randomUniversal);
				
				// create a random training data
				ArrayList<Example> randTraining = createRandomTraining(randomUniversal, numOfTrainExamples);
				// create a random evaluation data
				randEvaluation.removeAll(randTraining);
				
				scenario.materializeRandomUniversalsToC(scenario, randTraining);
				scenario.materializeRandTestExamplesToTest(scenario, randEvaluation);
				System.out.println("(Finished).");
				System.out.println(String.format("# training examples = %d,\t # test examples = %d", 
						randTraining.size(),
						randEvaluation.size()));
				
				
				/**
				 * Test code: trying to understand how many homomorphisms exists from the LHS of 
				 * goal mapping to the just created training set
				 */
				GAVmapping goal = scenario.getGoalMapping();
				double sizeOfGoalMapping = goal.gavExamples.size();
				long startOfChecking = System.currentTimeMillis();
				System.out.println("checking training comprehensiveness");
				HashSet<GAVconstraint> triggered_in_training   = getTriggeredConstraintsOptimizied(goal, randTraining);
				long endOfChecking = System.currentTimeMillis();
				System.out.println("TRIGGERED = " + triggered_in_training.size());
				System.out.println("Checking finished in = "+ (endOfChecking-startOfChecking)/1000.0);
//				System.out.println("checking evaluation comprehensiveness");
//				HashSet<GAVconstraint> triggered_in_evaluation = getTriggeredConstraintsOptimizied(goal, randEvaluation);
//
//				triggered_in_evaluation.removeAll(triggered_in_training);
//				boolean isTrainComprehensive = triggered_in_evaluation.size() > 0 ? false : true;
				
				learner = new GavLearner(pathToProperties);
				learner.setOptimizedPOstgresConnector(postgresConnector);

				learner.setPathToUniversalExamples(scenario.getPathToC());
				learner.setGoalMapping(scenario.getGoalMapping());
				learner.initialization();
				learner.setMaxIteration(learner.getMAX_LEARNING_ITERATION());
				long start = System.currentTimeMillis();
				learner.learn();
				long end = System.currentTimeMillis();
//				System.out.println("RUNTIME = " + (end-start));
				System.out.println(String.format("The %d-th learning has finished", (k+1)));
				
				GAVmapping learnedMapping = learner.getCandidate();
				HashSet<GavExample> mappingAsExamples = learnedMapping.gavExamples;
				storeGavMappingAsExamples(mappingAsExamples, scenario, pathToLearnedMappings);
//				System.out.println(learnedMapping);
				
				String pathToLearned =  pathToLearnedMappings +"learnedMapping";
				String pathToTestSet =  "/home/kun/code/bitbucketRepo/"
						+ "duoduo5813-gav_learning-dddf27358125/GAVLearning/src/RANDTEST";
				
				System.gc();
				mappingEvaluator evaluator = new mappingEvaluator(pathToTestSet, pathToLearned);
				LearningResult lr = evaluator.computePrecision_Recall_Fscore();
				lr.setRuntime((end-start));
				double degreeOfComprehensive_Training   = (double)triggered_in_training.size() / sizeOfGoalMapping;
//				double degreeOfComprehensive_Evaluation = (double)triggered_in_evaluation.size() / sizeOfGoalMapping;
				lr.setDegreeOfComp_Training(degreeOfComprehensive_Training);
//				lr.setDegreeOfComp_Evaluation(degreeOfComprehensive_Evaluation);
				System.out.println(lr);
//				lr.setTrainComprehensive(isTrainComprehensive);
				learningResults.add(lr);
			}
			
			System.out.println("\n\nLEARNING DONE");
			System.out.println(scenario.getParameters());
			for(LearningResult lr : learningResults)
			{
				System.out.println(lr);
			}
			double avgPrecision = LearningResult.avgPrecision(learningResults);
			double avgRecall    = LearningResult.avgRecall(learningResults);
			
			System.out.print(String.format("avg_Prec=%.3f", avgPrecision));
			System.out.print(String.format("\tavg_Recall=%.5f", avgRecall));
			System.out.print(String.format("\tF-score=%.5f", LearningResult.computeFscore(avgPrecision, avgRecall)));
			System.out.print(String.format("\tavg_DOC=%.5f", 
					LearningResult.avgDegreeOfComprehensiveness(learningResults)));
			System.out.print(String.format("\tDeviation_Prec=%.3f", LearningResult.computePrecisionDeviation(learningResults, avgPrecision)));
			System.out.print(String.format("\tDeviation_Recall=%.3f", LearningResult.computeRecallDeviation(learningResults, avgRecall)));
			System.out.println(String.format("\tavg_Runtime=%s", LearningResult.avgRuntime(learningResults)));

//			System.out.print( + avgPrecision);
//			System.out.print("\tavg_Recall= " + avgRecall);
//			System.out.print("\tF-score= " 
//					+ LearningResult.computeFscore(LearningResult.avgPrecision(learningResults), LearningResult.avgRecall(learningResults)));
//			System.out.println("\tavg_Runtime= " + LearningResult.avgRuntime(learningResults));
//			System.out.println("\tavg_DOC= " + LearningResult.avgDegreeOfComprehensiveness(learningResults));
//			System.out.println("Precision Standard Deviation= " + LearningResult.computePrecisionDeviation(learningResults, avgPrecision));
//			System.out.println("Recall Standard Deviation= " + LearningResult.computeRecallDeviation(learningResults, avgRecall));

			postgresConnector.closeUp();
		}

	}
	
	public static HashMap<GavExample, GAVconstraint> triggeredMap = new HashMap<GavExample, GAVconstraint>();
	public static HashSet<GAVconstraint> getTriggeredConstraintsOptimizied(GAVmapping goal, ArrayList<Example> examples)
	{
		ArrayList<GavExample> triggered = new ArrayList<GavExample>();
		HashSet<GAVconstraint> triggeredConstraint = new HashSet<GAVconstraint>();

		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		
		for(GavExample c : goal.gavExamples)
		{
			GAVmapping dummyMapping = new GAVmapping();
			dummyMapping.addConstraint(c);
			boolean isTriggered = false;
//			System.out.println("current Constraint = " + c);

			if(triggeredMap.containsKey(c))
			{
//				System.out.println("found a tgd that has been previously triggered");
				triggeredConstraint.add(triggeredMap.get(c));
				continue;
			}
			for(Example example : examples)
			{
				ArrayList<Fact> tmp = new ArrayList<Fact>();
				tmp.addAll(example.getSource().getFacts());
				Example newC = new Example();
				Instance newSource = new Instance(tmp);
				Instance newTarget = new Instance(new ArrayList<Fact>());
				newC.setSource(newSource);
				newC.setTarget(newTarget);
				
				postgresConnector.populateNewTableDDL(newC);
//				System.out.println("NewC= " + newC);
				HashSet<Fact> chased = GAVmapping.chase(dummyMapping, newC);
				if(chased.size()>0)
				{
//					System.out.println("This constraint IS triggered = " + c);
					isTriggered = true;
					triggered.add(c);
					triggeredConstraint.add(GAVconstraint.createConstraint(c));
					triggeredMap.put(c,GAVconstraint.createConstraint(c));
					break;
				}
			}
			if(!isTriggered)
			{
//				System.out.println("This constraint is not triggered = " + c);
			}
		}
		
		postgresConnector.closeUp();
		
		return triggeredConstraint;
	}
	
	public static ArrayList<GAVconstraint> getTriggeredConstraints(GAVmapping goal, ArrayList<Example> examples)
	{
		ArrayList<GavExample> triggered = new ArrayList<GavExample>();
		ArrayList<GAVconstraint> triggeredConstraint = new ArrayList<GAVconstraint>();

		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		
		for(GavExample c : goal.gavExamples)
		{
			GAVmapping dummyMapping = new GAVmapping();
			dummyMapping.addConstraint(c);
			boolean isTriggered = false;
//			System.out.println("current Constraint = " + c);

			for(Example example : examples)
			{
				ArrayList<Fact> tmp = new ArrayList<Fact>();
				tmp.addAll(example.getSource().getFacts());
				Example newC = new Example();
				Instance newSource = new Instance(tmp);
				Instance newTarget = new Instance(new ArrayList<Fact>());
				newC.setSource(newSource);
				newC.setTarget(newTarget);
				
				postgresConnector.populateNewTableDDL(newC);
//				System.out.println("NewC= " + newC);
				HashSet<Fact> chased = GAVmapping.chase(dummyMapping, newC);
				if(chased.size()>0)
				{
//					System.out.println("This constraint IS triggered = " + c);
					isTriggered = true;
					triggered.add(c);
					triggeredConstraint.add(GAVconstraint.createConstraint(c));
					break;
				}
			}
			if(!isTriggered)
			{
//				System.out.println("This constraint is not triggered = " + c);
			}
		}
		
		postgresConnector.closeUp();
		
		return triggeredConstraint;
	}
	
	public static boolean checkComprehensivenessForEvaluation(GAVmapping goal, ArrayList<Example> train)
	{
		boolean isComprehensive = true;
		OptimizedPostgresConnector postgresConnector = new OptimizedPostgresConnector();
		postgresConnector.initialization();
		
		for(GAVconstraint c : goal.gavConstraints)
		{
			GAVmapping dummyMapping = new GAVmapping();
			dummyMapping.addConstraint(c);
			boolean isTriggered = false;
			for(Example example : train)
			{
				ArrayList<Fact> tmp = new ArrayList<Fact>();
				tmp.addAll(example.getSource().getFacts());
				Example newC = new Example();
				Instance newSource = new Instance(tmp);
				Instance newTarget = new Instance(new ArrayList<Fact>());
				newC.setSource(newSource);
				newC.setTarget(newTarget);
				
				postgresConnector.populateNewTableDDL(newC);
				HashSet<Fact> chased = GAVmapping.chase(dummyMapping, newC);
				if(chased.size()>0)
				{
					isTriggered = true;
					break;
				}
			}
			if(!isTriggered)
			{
				System.out.println("This constraint is not triggered in the evaluation data = " + c);
				postgresConnector.closeUp();
				return false;
			}
		}
		
		postgresConnector.closeUp();
		
		return isComprehensive;
	}
	
	public static ArrayList<Example> createRandomTraining(ArrayList<Example> pool, int numOfTraining)
	{
		ArrayList<Example> randResult = new ArrayList<Example>();
		ArrayList<Example> copy = new ArrayList<Example>(pool);
		
		while(numOfTraining>0)
		{
			int remainingSize = copy.size();
			int index = (int)(Math.random()*remainingSize);
			randResult.add(copy.get(index));
			copy.remove(index);			
			numOfTraining--;
		}		
		return randResult;
	}
	
	public static void storeGavMappingAsExamples(HashSet<GavExample> learnedMapping, 
			iBenchMappingScenario scenario, String path)
	{
		StringBuffer sb = new StringBuffer();

		for (GavExample example : learnedMapping) {
//			System.out.println("Storing = "+ example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(scenario.formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(example.getTargetFact());
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
//		System.out.println(sb.toString());
		try {
			FileWriter writer = new FileWriter(path+"learnedMapping");
			writer.append(sb.toString());
			writer.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static ArrayList<Example> createTrainingSet(ArrayList<Example> allE, int size)
	{
		ArrayList<Example> result = new ArrayList<Example>();
		int count = 0; 
		while(count < size)
		{
			result.add(allE.get(count));
			count++;
		}
		return result;
	}
	public static ArrayList<Example> createTestset(ArrayList<Example> allE, int size)
	{
		ArrayList<Example> result = new ArrayList<Example>();
		for(int k = allE.size()-1; size > 0; k--)
		{
			result.add(allE.get(k));
			size--;
		}
		return result;
	}
	
	public static void cleanRandomInstances()
	{
		try 
		{
			ProcessBuilder pb = new ProcessBuilder("rm", "-r", "out0/");
			pb.directory(new File("/kun/coding/ibench/build/mac/random"));
			Process p = pb.start();
		    p.waitFor();
		    StringBuffer sb = new StringBuffer();
		    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

		    String line = "";			
		    while ((line = reader.readLine())!= null) {
		    	sb.append(line + "\n");
		    }
		    
		    System.out.println(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void cleanComprehensiveInstances()
	{
		try 
		{
			ProcessBuilder pb = new ProcessBuilder("rm", "-r", "out0/");
			pb.directory(new File("/kun/coding/ibench/build/mac/comprehensive"));
			Process p = pb.start();
		    p.waitFor();
		    StringBuffer sb = new StringBuffer();
		    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

		    String line = "";			
		    while ((line = reader.readLine())!= null) {
		    	sb.append(line + "\n");
		    }
		    
		    System.out.println(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void runIBENCH(String folder, String configFile, int iteration)
	{
		try 
		{
			// sh iBenchMac.sh -c gavtest0
//			System.out.println(folder);
			ProcessBuilder pb = new ProcessBuilder("sh", Keywords.IBENCH_SHELL_SCRIPT_NAME,
									"-c", configFile);
			pb.directory(new File(folder));
			System.out.println("folder = " + folder);

			Process p = pb.start();
		    p.waitFor();
		    StringBuffer sb = new StringBuffer();
		    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

		    String line = "";			
		    while ((line = reader.readLine())!= null) {
		    	sb.append(line + "\n");
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
