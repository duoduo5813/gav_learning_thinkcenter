package com.learning.learn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.learning.example.Example;
import com.learning.example.*;
import com.learning.mapping.*;

public class PostgresConnector 
{
	/**
	 *  start postgresql manually
	 *  pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start

		stop postgresql manually
		pg_ctl -D /usr/local/var/postgres stop -s -m fast
	 * @param example
	 * @return
	 */
	public static void resetGavLearningDB()
	{
		String dropDB   = "DROP DATABASE IF EXISTS " + Param.POSTGRESQL_DATABASE;
		String createDB = "CREATE DATABASE "+ Param.POSTGRESQL_DATABASE ;
		try 
		{
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection(
					Param.POSTGRESQL_COMPLETE_CONNECTION_TO_CRITICAL, 
					Param.POSTGRESQL_USERNAME,
					Param.POSTGRESQL_PASSWORD);
			System.out.println("Dropping = " + dropDB);
			Statement stmt = connection.createStatement();
		    stmt.executeUpdate(dropDB);
		    PreparedStatement ps = connection.prepareStatement(dropDB);
			ps.executeUpdate();
			ps = connection.prepareStatement(createDB);
			ps.executeUpdate();
			ps.close();
			connection.close();
		System.out.println(Param.POSTGRESQL_DATABASE + " has been successfully reset!");

		} 
		catch (ClassNotFoundException e) {
//			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static String populateNewTableForCritical(Example example)
	{
		// sample script
		// CREATE TABLE IF NOT EXISTS table_name (attr1 VARCHAR(128), attr2 VARCHAR(128... )
		String createTabelScriptTemplate = "CREATE TABLE IF NOT EXISTS %s (%s)";
		String dropTabelScriptTemplate = "DROP TABLE IF EXISTS %s";
		String dropDBScriptTemplate = "DROP DATABASE IF EXISTS %s";
		String createDBScriptTemplate = "CREATE DATABASE %s";
		
		String insertTableValuesTemplate = "INSERT INTO %s VALUES(%s)";
		Instance sourceInstance = example.getSource();
		HashMap<String,ArrayList<Fact>> factMap = sourceInstance.getFactMap();
		String tableTag = example.getDatasourceTag();
		
		for(String relation : factMap.keySet())
		{
			// first create table if not exist
			ArrayList<Fact> allFacts = factMap.get(relation);
			String tableName = relation+tableTag;
			StringBuffer schemaList = new StringBuffer();
			int arity = allFacts.get(0).getArity();
			for(int i = 1; i<= arity; i++)
			{
				// attri VARCHAR(128)
				schemaList.append(Param.TABLE_ATTRIBUTE_PREFIX+i);
				schemaList.append(Param.KEYWORD_VARCHAR);
				schemaList.append(",");
			}
			String schemaDefinitionClause = schemaList.deleteCharAt(schemaList.length()-1).toString();
			
			String newTableScript = String.format(createTabelScriptTemplate, 
					tableName,
					schemaDefinitionClause);
			String dropTableScript = String.format(dropTabelScriptTemplate,  tableName);
			
			String dropDBScript   = String.format(dropDBScriptTemplate,  Param.POSTGRESQL_CRITICAL_DATABASE);
			String createDBScript = String.format(createDBScriptTemplate,  Param.POSTGRESQL_CRITICAL_DATABASE);

			createNewTablesCritical(dropDBScript, createDBScript, newTableScript);
			
			
			// second populate 
			for(Fact fact : allFacts)
			{
				String[] vals = fact.getTuple();
				StringBuffer values = new StringBuffer();
				for(String val : vals)
				{
					values.append(String.format("\'%s\'", val));
					values.append(",");
				}
				String valueList = values.deleteCharAt(values.length()-1).toString();
				String insertValueScript = String.format(insertTableValuesTemplate, 
						tableName,
						valueList);
//				System.out.println("insertValueScript = " + insertValueScript);
				insertValueToTableCritical(insertValueScript);
			}
		}
		
//		System.out.println("currentFactMap="+factMap+" tag="+ tableTag+ " arity=");
		return "";
	}
	
	public static void createNewTablesCritical(String dropDB, String createDB, String newTableScript)
	{
		try 
		{
			Class.forName("org.postgresql.Driver");
//			System.out.println("PostgreSQL JDBC Driver Registered!");
			Connection connection = null;
			connection = DriverManager.getConnection(
					Param.POSTGRESQL_COMPLETE_CONNECTION, 
					Param.POSTGRESQL_USERNAME,
					Param.POSTGRESQL_PASSWORD);
//			System.out.println("Dropping = " + dropDB);
			PreparedStatement ps = connection.prepareStatement(dropDB);
			ps.executeUpdate();
			ps = connection.prepareStatement(createDB);
			ps.executeUpdate();
			ps = connection.prepareStatement(newTableScript);
			ps.executeUpdate();
			
//			System.out.println("New table has been successfully created");
			ps.close();
			connection.close();
			
		} 
		catch (ClassNotFoundException e) {
//			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void insertValueToTableCritical(String insertValueScript)
	{
		try 
		{
			Class.forName("org.postgresql.Driver");
//			System.out.println("PostgreSQL JDBC Driver Registered!");
			Connection connection = null;
			connection = DriverManager.getConnection(
					Param.POSTGRESQL_COMPLETE_CONNECTION_TO_CRITICAL, 
					Param.POSTGRESQL_USERNAME,
					Param.POSTGRESQL_PASSWORD);
			PreparedStatement ps = connection.prepareStatement(insertValueScript);
			ps.executeUpdate();
//			System.out.println("New value has been successfully inserted");
			ps.close();
			connection.close();
			System.gc();
		} 
		catch (ClassNotFoundException e) {
//			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String populateNewTableDDL(Example example)
	{
		// sample script
		// CREATE TABLE IF NOT EXISTS table_name (attr1 VARCHAR(128), attr2 VARCHAR(128... )
		String createTabelScriptTemplate = "CREATE TABLE IF NOT EXISTS %s (%s)";
		String dropTabelScriptTemplate = "DROP TABLE IF EXISTS %s";
		String insertTableValuesTemplate = "INSERT INTO %s VALUES(%s)";
		Instance sourceInstance = example.getSource();
		HashMap<String,ArrayList<Fact>> factMap = sourceInstance.getFactMap();
		String tableTag = example.getDatasourceTag();
		
		for(String relation : factMap.keySet())
		{
			// first create table if not exist
			ArrayList<Fact> allFacts = factMap.get(relation);
			String tableName = relation+tableTag;
			StringBuffer schemaList = new StringBuffer();
			int arity = allFacts.get(0).getArity();
			for(int i = 1; i<= arity; i++)
			{
				// attri VARCHAR(128)
				schemaList.append(Param.TABLE_ATTRIBUTE_PREFIX+i);
				schemaList.append(Param.KEYWORD_VARCHAR);
				schemaList.append(",");
			}
			try
			{
				String schemaDefinitionClause = schemaList.deleteCharAt(schemaList.length()-1).toString();
				
				String newTableScript = String.format(createTabelScriptTemplate, 
						tableName,
						schemaDefinitionClause);
				String dropTableScript = String.format(dropTabelScriptTemplate,  tableName);
				
				createNewTables(dropTableScript, newTableScript);
				
				
				// second populate 
				for(Fact fact : allFacts)
				{
					String[] vals = fact.getTuple();
					StringBuffer values = new StringBuffer();
					for(String val : vals)
					{
						values.append(String.format("\'%s\'", val));
						values.append(",");
					}
					String valueList = values.deleteCharAt(values.length()-1).toString();
					String insertValueScript = String.format(insertTableValuesTemplate, 
							tableName,
							valueList);
//					System.out.println("insertValueScript = " + insertValueScript);
					insertValueToTable(insertValueScript);
				}
			}
			catch(Exception e)
			{
//				System.out.println("has problem with example " + example);
			}
		}
		
//		System.out.println("currentFactMap="+factMap+" tag="+ tableTag+ " arity=");
		return "";
	}
	
	public static void insertValueToTable(String insertValueScript)
	{
		try 
		{
			Class.forName("org.postgresql.Driver");
//			System.out.println("PostgreSQL JDBC Driver Registered!");
			Connection connection = null;
			connection = DriverManager.getConnection(
					Param.POSTGRESQL_COMPLETE_CONNECTION, 
					Param.POSTGRESQL_USERNAME,
					Param.POSTGRESQL_PASSWORD);
			PreparedStatement ps = connection.prepareStatement(insertValueScript);
			ps.executeUpdate();
//			System.out.println("New value has been successfully inserted");
			ps.close();
			connection.close();
			System.gc();
			
		} 
		catch (ClassNotFoundException e) {
//			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void createNewTables(String dropTableScript, String newTableScript)
	{
		try 
		{
			Class.forName("org.postgresql.Driver");
//			System.out.println("PostgreSQL JDBC Driver Registered!");
			Connection connection = null;
			connection = DriverManager.getConnection(
					Param.POSTGRESQL_COMPLETE_CONNECTION, 
					Param.POSTGRESQL_USERNAME,
					Param.POSTGRESQL_PASSWORD);
//			System.out.println("Dropping = " + dropTableScript);
			PreparedStatement ps = connection.prepareStatement(dropTableScript);
			ps.executeUpdate();
			ps = connection.prepareStatement(newTableScript);
			ps.executeUpdate();
			
//			System.out.println("New table has been successfully created");
			ps.close();
			connection.close();
			connection = null;
			System.gc();

		} 
		catch (ClassNotFoundException e) {
//			System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
			e.printStackTrace();
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
