package com.learning.learn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;

import com.learning.example.Example;
import com.learning.example.Fact;
import com.learning.example.GavExample;
import com.learning.ibench.adapters.iBenchMappingParser;
import com.learning.ibench.adapters.iBenchMappingScenario;
import com.learning.mapping.GAVmapping;
import com.learning.mapping.Param;

public class learnFromMultiComprehensiveMain {
	
	final static int evaluationID = 1;
	final static int numOfComprehensive = 5;
	final static String pathToLearnedMappings = "/Users/kunqian/git/gav_learning/GAVLearning/learnedMappings/";
	public static void main(String[] args)
	{
		cleanComprehensiveInstances();
		String pathToProperties = "/Users/kunqian/git/gav_learning/GAVLearning/learningConfig";
		iBenchMappingParser iBenchParser = new iBenchMappingParser(pathToProperties);
		GavLearner learner = new GavLearner(pathToProperties);

		int    numOfExamples 			= learner.getNUMBER_OF_RANDOM_UNIVERSAL_EXAMPLES();
		double selectivityLowerBound 	= learner.getRANDOM_SELECTIVITY_LOWERBOUND();
		double testsetPerc 				= (double)(learner.getTESTSET_PERCENTAGE())/100f;
		int numOfTrainExamples    		= (int)(numOfExamples*testsetPerc);
		
		iBenchMappingScenario scenario = iBenchParser.parse();
		scenario.setPropertiesObject(pathToProperties);
		
		// Step 1. Fix the mapping, and then create a configuration file for a new comprehensive source instance
		scenario.buildConfigFileForiBench(iBenchParser.getIBENCH_CONFIGFILE_TEMPLATE_PATH(), 
							iBenchParser.getIBENCH_ROOT(), evaluationID);		
		
		ArrayList<Example> allCompSources = new ArrayList<Example>();
		
		// step 2. Run iBench a number of times to get different comprehensive source instances
	    System.out.print("Constructing source instance.\n");
		for(int comprehensiveID = 1; comprehensiveID <= numOfExamples; comprehensiveID++)
		{
			System.out.print("Creating "+ comprehensiveID+"-th\tSource Instance ");
			String newConfigFileName = Keywords.COMPREHENSIVE_SCENARIOS_FOLDER+
					"/"+Keywords.CONFIG_FILENAME+evaluationID;
			String workFolder  		 = iBenchParser.getIBENCH_ROOT();
			runIBENCH(workFolder, newConfigFileName,  comprehensiveID);
		    
		    
//		    System.out.print("1.Parsing iBench Mapping Scenario ");
		    iBenchParser.TriggerComprehensive(true);
		    scenario = iBenchParser.parse();
			scenario.setPropertiesObject(pathToProperties);
			scenario.outputScenario();
//			System.out.println("(Finished).");

			
			Example comprehensive = iBenchMappingScenario.createExample(scenario);
			allCompSources.add(comprehensive);
//			System.out.println("New Compreh. instance =" + comprehensive);
			System.out.println("(finished)");
			
		}
		ArrayList<Example> allComprehensives = 
				scenario.createComprehensiveUniversalExamples(scenario.getGoalMapping(), allCompSources);
	    System.out.println(allComprehensives.size() + " universal examples have been constructed (Finished)");

		System.out.print("3.Materializing Training Data and test data to files ");
		ArrayList<Example> trainingSet = createTrainingSet(allComprehensives, numOfTrainExamples);
		ArrayList<Example> testSet	   = createTrainingSet(allComprehensives, numOfExamples-numOfTrainExamples);
		scenario.materializeRandomUniversalsToC(scenario, trainingSet);
		scenario.materializeTestExamplesToTest(scenario, testSet);
		System.out.println("(Finished).");
		
		System.out.println(String.format("# training examples = %d,\t # test examples = %d", trainingSet.size(),
				testSet.size()));
		
		System.out.println("Goal Mapping = " + scenario.getGoalMapping());
		
		learner.setPathToUniversalExamples(scenario.getPathToC());
		learner.setGoalMapping(scenario.getGoalMapping());
		learner.initialization();
		learner.setMaxIteration(learner.getMAX_LEARNING_ITERATION());
		learner.learn();
		
		GAVmapping learnedMapping = learner.getCandidate();
		HashSet<GavExample> mappingAsExamples = learnedMapping.gavExamples;
		storeGavMappingAsExamples(mappingAsExamples, scenario, pathToLearnedMappings);
		System.out.println(learnedMapping);
		
		
	}
	
	public static void storeGavMappingAsExamples(HashSet<GavExample> learnedMapping, 
			iBenchMappingScenario scenario, String path)
	{
		StringBuffer sb = new StringBuffer();

		for (GavExample example : learnedMapping) {
			System.out.println("Storing = "+ example);
			// create a new universal example
			sb.append(Param.UNIVERSAL_EXAMPLE_DELIMITER);
			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_SOURCE_INSTANCE_TAG);
			sb.append(scenario.formattingUniversalInstance(example.getSource().getFacts()));

			sb.append(Param.NEWLINE);
			sb.append(Param.UNIVERSAL_TARGET_INSTANCE_TAG);
			sb.append(example.getTargetFact());
			sb.append(Param.NEWLINE);
			sb.append(Param.NEWLINE);
		}
		System.out.println(sb.toString());
		try {
			FileWriter writer = new FileWriter(path+"learnedMapping");
			writer.append(sb.toString());
			writer.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static ArrayList<Example> createTrainingSet(ArrayList<Example> allE, int size)
	{
		ArrayList<Example> result = new ArrayList<Example>();
		int count = 0; 
		while(count < size)
		{
			result.add(allE.get(count));
			count++;
		}
		return result;
	}
	public static ArrayList<Example> createTestset(ArrayList<Example> allE, int size)
	{
		ArrayList<Example> result = new ArrayList<Example>();
		for(int k = allE.size()-1; size > 0; k--)
		{
			result.add(allE.get(k));
			size--;
		}
		return result;
	}
	
	public static void cleanComprehensiveInstances()
	{
		try 
		{
			ProcessBuilder pb = new ProcessBuilder("rm", "-r", "out0/");
			pb.directory(new File("/kun/coding/iBench/build/mac/comprehensive"));
			Process p = pb.start();
		    p.waitFor();
		    StringBuffer sb = new StringBuffer();
		    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

		    String line = "";			
		    while ((line = reader.readLine())!= null) {
		    	sb.append(line + "\n");
		    }
		    
		    System.out.println(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void runIBENCH(String folder, String configFile, int iteration)
	{
		try 
		{
			// sh iBenchMac.sh -c gavtest0
//			System.out.println(folder);
			ProcessBuilder pb = new ProcessBuilder("sh", Keywords.IBENCH_SHELL_SCRIPT_NAME,
									"-c", configFile);

			pb.directory(new File(folder));
			System.out.println("folder = " + folder);

			Process p = pb.start();
		    p.waitFor();
		    StringBuffer sb = new StringBuffer();
		    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

		    String line = "";			
		    while ((line = reader.readLine())!= null) {
		    	sb.append(line + "\n");
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
}
